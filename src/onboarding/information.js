import React, { Component, PropTypes } from 'react'
import UserInfo from './user-info'
import * as Exceptions from '../error-codes'
import { styles as Styles, theme_vars as Theme } from '../styles'

import {
  Keyboard,
  StyleSheet,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Information extends Component {
  constructor(props) {
    super(props)
  }

	shouldComponentUpdate(nextProps) {
		return false
	}

  render() {
    return (
			<TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
				<View style={this.props.style}>
					<UserInfo style={styles.form} onComplete={this.props.onComplete}/>
				</View>
			</TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
	signup: {
    color: '#fff'
  },
	signupLink: {
    color: '#0774FA'
  },
	signupAction: {
		justifyContent: 'center',
    alignItems: 'center',
    width: 350,
		height: 20
  },
	errorTxt: {
    color: '#ff0000'
  },
  form: {
		flex: 1,
		padding: 0
  }
});
