/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import * as Exceptions from '../error-codes'
import SummaryRowItem from './summary-item'
import { fromJS, List, Map, toJS } from 'immutable'
import { styles as Styles, CategoriesStyle, SummaryStyle } from '../styles'
import Icon from 'react-native-vector-icons/Ionicons'

import {
	ListView,
	ScrollView,
  StyleSheet,
  Text,
	TextInput,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Summary extends Component {
	static propTypes = {
		schemaCategories: PropTypes.object.isRequired,
		schemaSubCategories: PropTypes.object.isRequired,
		subcategories: PropTypes.object.isRequired,
		customSubcategories: PropTypes.object.isRequired,
		userData: PropTypes.object.isRequired,
	}

  constructor(props) {
    super(props)

		this.state = {
			dataSource: new ListView.DataSource({
	      rowHasChanged: (row1, row2) => row1 !== row2,
	    }),
    }
  }

	componentDidMount() {
		let rows = []
		const { schemaCategories, subcategories, customSubcategories } = this.props

		subcategories.map(subcat => {
			rows.push({
				subcategory: subcat.name
			})
		})

		customSubcategories.map(custom => {
			rows.push({
				subcategory: custom.get('value')
			})
		})

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(rows)
    })
  }

	renderCategory = (item) => {
		return (
			<SummaryRowItem
				containerStyle={Styles.helpContainerStyle}
				item={item}/>
		)
	}

	renderUserData() {
		const { userData } = this.props
		return (
			<View style={SummaryStyle.userDataContainer}>
				<Text style={SummaryStyle.userData}>
					<Text style={SummaryStyle.userDataLbl}>Name: </Text>
					{userData.last_name}, {userData.first_name}
				</Text>
				<Text style={SummaryStyle.userData}>
					<Text style={SummaryStyle.userDataLbl}>DOB: </Text>
					{userData.birthday.toLocaleDateString("en-US")}
				</Text>
				<Text style={SummaryStyle.userData}>
					<Text style={SummaryStyle.userDataLbl}>Weight: </Text>
					{userData.weight}
				</Text>
				<Text style={SummaryStyle.userData}>
					<Text style={SummaryStyle.userDataLbl}>Gender: </Text>
					{userData.gender}
				</Text>
				<Text style={SummaryStyle.userData}>
					<Text style={SummaryStyle.userDataLbl}>Months in Recovery: </Text>
					{userData.recovery_months}
				</Text>
			</View>
		)
	}

  render() {
    return (
			<View style={this.props.style}>
				<ScrollView
					automaticallyAdjustContentInsets={false}
					scrollEventThrottle={200}
					style={Styles.ftuxContainer}>

					<ListView
						dataSource={this.state.dataSource}
						renderRow={this.renderCategory}
					/>
				</ScrollView>
			</View>
    )
  }
}
