/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import { styles, constants } from '../styles'

import {
  Image,
  Switch,
  Text,
  View
} from 'react-native'

export default class SubCategory extends Component {
	state = {
		checked: false
	}

  static defaultProps = {
    disabled: PropTypes.bool.isRequired
  }

	onCheck = (value) => {
		this.setState({checked: value})
		this.props.onPress(value, this.props.item)
	}

  render() {
    return (
      <View style={styles.li}>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Text style={styles.liText}>
            {this.props.item.name}
          </Text>
          <Switch
            style={{marginRight: 10, marginLeft: 10}}
            onValueChange={this.onCheck}
            value={this.state.checked}
            disabled={this.props.item.disabled} />
        </View>
      </View>
    )
  }
}
