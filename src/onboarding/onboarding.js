/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import Swiper from 'react-native-swiper'
import PushNotification from 'react-native-push-notification'
import firebase from 'firebase'
import {
  AppRegistry,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
  View,
  Image
} from 'react-native'

export default class Onboarding extends Component {
  static propTypes = {
    onFinishOnboarding: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    firebase.database().ref(`/users/${firebase.auth().currentUser.uid}`).once('value').then(snapshot => {
      const pn_token = snapshot.val().pn_token

      if(!pn_token) {
        PushNotification.configure({
          onRegister: function(token) {
            firebase.database().ref(`/users/${firebase.auth().currentUser.uid}`).update({pn_token: token})
          },

          onNotification: (notification) => {
            console.log( 'NOTIFICATION:', notification );
          },

          // ANDROID ONLY
          senderID: "YOUR GCM SENDER ID",

          // IOS ONLY (optional): default: all - Permissions to register.
          permissions: {
              alert: true,
              badge: true,
              sound: true
          },

          // Should the initial notification be popped automatically
          // default: true
          popInitialNotification: true,

          /**
            * (optional) default: true
            * - Specified if permissions (ios) and token (android and ios) will requested or not,
            * - if not, you must call PushNotificationsHandler.requestPermissions() later
            */
          requestPermissions: true,
        })

        PushNotification.localNotificationSchedule({
          /* Android Only Properties */
          date: new Date(),
          ticker: "Daily PCI Reminder", // (optional)
          autoCancel: true, // (optional) default: true
          largeIcon: "ic_launcher", // (optional) default: "ic_launcher"
          smallIcon: "ic_notification", // (optional) default: "ic_notification" with fallback for "ic_launcher"
          vibrate: true, // (optional) default: true
          vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
          ongoing: false, // (optional) set whether this is an "ongoing" notification

          /* iOS and Android properties */
          title: "Daily PCI Reminder", // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
          message: "Don't forget to do stuff :smiley:", // (required)
          playSound: true, // (optional) default: true
          soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
          number: 10, // (optional) Valid 32 bit integer specified as string. default: none (Cannot be zero)
          repeatType: 'minute', // (Android only) Repeating interval. Could be one of `week`, `day`, `hour`, `minute, `time`. If specified as time, it should be accompanied by one more parameter 'repeatTime` which should the number of milliseconds between each interval
          repeatInterval:'minute', // IOS only
        });
      }
    })
  }

  render() {
    return (
      <Swiper style={styles.wrapper} loop={false}>
          <View style={{flex: 1, backgroundColor: '#904ad2'}} key={0}>
              <View style={{ flex: 1/3, alignItems: 'center',
              justifyContent: 'center'}}>
                  <Text style={styles.largeText}>
                    Welcome to Personal
                  </Text>
                  <Text style={styles.largeText}>
                    Craziness Index!
                  </Text>
              </View>
              <View style={{ flex: 1/3, alignItems: 'center',
              justifyContent: 'center'}}>
                  <Image
                  source={require( '../../assets/welcome_pci_logo.png')}
                  style={styles.backgroundImage} />
              </View>
              <View style={{ flex: 1/3,
              justifyContent: 'center', alignItems: 'center',
              marginLeft: 40, marginRight: 40 }}>
                <Text style={styles.text}>
                  The first set of screens contains
                  visual instructions to help you get set up quickly.
                </Text>
                <Text style={styles.text}></Text>
                <Text style={styles.text}>
                  The second set contains the forms for setting up
                  your profile.
                </Text>
                <Text style={styles.text}></Text>
                <View style={{alignItems: 'center'}}>
                  <Text style={styles.text}>
                    (Swipe Left!)
                  </Text>
                </View>
              </View>
          </View>

          <View style={styles.slidetop} key={1}>
              <Image source={ require( '../../assets/instruction_info.png')} />
          </View>
          <View style={styles.slidetop} key={2}>
              <Image source={ require( '../../assets/instruction_cat.png')} />
          </View>
          <View style={styles.slidetop} key={3}>
              <Image source={ require( '../../assets/instruction_subcat.png')} />
          </View>
          <View style={styles.slidetop} key={4}>
              <Image source={ require( '../../assets/instruction_summary.png')} />
          </View>
          <View style={styles.slidecenter} key={5}>
              <View style={{flex: 1/2}}>
                  <Image source={ require( '../../assets/getstarted.png')} style={styles.backgroundImage} />
              </View>
              <View style={{flex: 1/2}}>
                  <TouchableHighlight onPress={this.props.onFinishOnboarding} style={styles.button}>
                      <Text style={styles.buttonText}>
                      Get Started
                    </Text>
                  </TouchableHighlight>
              </View>
          </View>
      </Swiper>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
  },
  backgroundImage: {
    flex: 1,
    width: 250,
    height: 250,
    resizeMode: 'contain', // or 'stretch'
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,

  },
  slidecenter: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: '#ff5c41',
  },
  slidetop: {
    flex: 1,
    paddingTop: 20,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingHorizontal: 15,
    backgroundColor: '#747474',
  },
  largeText: {
    color: '#fff',
    fontSize: 26,
    fontWeight: 'bold',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    color: '#fff',
    fontSize: 14,
    fontWeight: 'bold',
  },
  button: {
		width: 150,
		height: 75,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#5872d3',
    borderRadius: 3,
		marginTop: 25
	},
	buttonText: {
		color: '#FFF',
    fontSize: 18
	}
});
