/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import * as Exceptions from '../error-codes'
import SubCategory from './subcategory'
import { fromJS, List, Map, toJS } from 'immutable'
import { styles as Styles, CategoriesStyle } from '../styles'
import Icon from 'react-native-vector-icons/Ionicons'
import InfoBox from '../components/info-box'

import {
	KeyboardAvoidingView,
	ListView,
	ScrollView,
  StyleSheet,
  Text,
	TextInput,
	TouchableOpacity,
  View
} from 'react-native'

/**
 * dataSources
 * [
 *   {
 *     category: {key: "cat1", title: "title"},
 *     subcategories: [
 *       {
 *         key: "subcat1",
 *			   title: "This is a title"
 *       },...
 *     ],...
 *   },...
 * ]
 */
export default class Categories extends Component {
	state = {
		addedCatKeys: List(),
		addedSubCat: List(),
		addedCustom: List(),
		dataSources: Map(),
	}

	static propTypes = {
		categories: PropTypes.object.isRequired,
		onComplete: PropTypes.func.isRequired,
	}

  constructor(props) {
    super(props)
  }

	componentDidMount() {
		this.generateDS()
	}

	generateDS(addedSubCatsList, addedCatKeysList) {
		let dataSources = []
		const addedCategories = this.props.categories.sort()
		let { addedSubCat:added, addedCatKeys:addedCat } = this.state

		if(addedSubCatsList && addedCatKeysList) {
			added = addedSubCatsList
			addedCat = addedCatKeysList
		}

		addedCategories.map(category => {
			let dataSource = {}
			let subCats = []

			dataSource['category'] = {
				key: category,
				name: this.props.schemaCategories[category].text,
			}

			fromJS(this.props.schemaSubCategories).entrySeq().toJS().map(subcategory => {
				const key = subcategory[0]
				const value = subcategory[1]
				if(subcategory[1].get('category') === category) {
					let isSelected = added.size > 0
					const foundCategory = addedCat.size > 0 && (addedCat.toArray().indexOf(category) > -1)

					if(added.size > 0) {
						isSelected = false
						for(const item of added) {
							if(item.key === key) {
								isSelected = true
								break;
							}
						}
					}

					subCats.push({
						key,
						name: value.get('text'),
						category,
						disabled: !isSelected && foundCategory,
					})
				}
			})

			const listDS = new ListView.DataSource({
	      rowHasChanged: (row1, row2) => row1 !== row2,
	    })

			dataSource['subcategories'] = listDS.cloneWithRows(subCats)
			dataSource['customSubcategories'] = List()
			dataSources.push(dataSource)
		})

    this.setState({ dataSources: fromJS(dataSources) })
	}

	/**@TODO change how we validate the onComplete from size > 0 to something else*/
	addSubCategory = (isAdded, item) => {
		const { addedSubCat, addedCatKeys } = this.state
		let newList = List(), newCatList = List()

		if(!isAdded) {
			newList = addedSubCat.delete(
				addedSubCat.findIndex(subcat => subcat.key === item.key)
			)

			newCatList = addedCatKeys.delete(
				addedCatKeys.findIndex(cat => cat === item.category)
			)
		} else {
			newList = addedSubCat.push(item)
			newCatList = addedCatKeys.push(item.category)
		}

		this.props.onComplete(newList.size + this.state.addedCustom.size === this.props.categories.size, newList, this.state.addedCustom)
		this.generateDS(newList, newCatList)
		this.setState({addedSubCat: newList, addedCatKeys: newCatList})
	}

	renderSubCategory = (item) => {
		return (
			<SubCategory
				containerStyle={Styles.helpContainerStyle}
				helpStyle={Styles.helpStyle}
				item={item}
				onPress={this.addSubCategory} />
		)
	}

	addCustomSubCategory = (index, category) => {
		const dataSources = this.state.dataSources.update(index, dataSource => {
			const customData = dataSource.get('customSubcategories')

			if(!customData.size) {
				return dataSource.set('customSubcategories', customData.push(''))
			} else {
				this.handleCustomSubcategory(null, category, false)
				return dataSource.set('customSubcategories', List())
			}
		})

		this.setState({ dataSources })
	}

	handleCustomSubcategory = (event, category, isAdd) => {
		const { addedCustom } = this.state
		let newAddedCustom = List()

		if(isAdd) {
			const value = event.nativeEvent.text
			const index = addedCustom.findIndex(custom => custom.get('category') === category)

			if(index > -1) {
				newAddedCustom = addedCustom.update(index, custom => {
					return custom.set('value', value)
				})
			} else {
				newAddedCustom = addedCustom.push(fromJS({category, value}))
			}
		} else {
			newAddedCustom = addedCustom.delete(addedCustom.findIndex(custom => custom.get('category') === category))
		}

		this.props.onComplete(this.state.addedSubCat.size + newAddedCustom.size === this.props.categories.size, this.state.addedSubCat, newAddedCustom)
		this.setState({addedCustom: newAddedCustom})
	}

	renderHelp() {
		return (
			<InfoBox
				containerStyle={{backgroundColor: 'rgba(0,0,0,0.7)'}}
				type='info'
				iconColor='#FFF'>
				<Text style={{color: '#FFF'}}>
					Remember to use a question which answers to "yes"
				</Text>
			</InfoBox>
		)
	}

	getListViews() {
		return this.state.dataSources.valueSeq().map((dataSource, key) => {
			const category = dataSource.get('category')
			const subcategories = dataSource.get('subcategories')
			const customSubcategories = dataSource.get('customSubcategories')
			const { addedCatKeys } = this.state
			const foundCategory = addedCatKeys.size > 0 && (addedCatKeys.toArray().indexOf(category.get('key')) > -1)
			const opacity = foundCategory? 0.2:1

			return (
				<View key={key}>
					<KeyboardAvoidingView behavior={'position'} keyboardVerticalOffset={100}>
						<Text style={Styles.liHeaderText}>{category.get('name')}</Text>
						<ListView
							initialListSize={1}
							dataSource={subcategories}
							renderRow={this.renderSubCategory} />
						{customSubcategories.valueSeq().map((value, key) => (
							<View key={key}>
								{this.renderHelp()}
								<TextInput
									onChange={event => this.handleCustomSubcategory(event, category, true)}
									autoFocus={true}
									style={styles.input}
									autoCapitalize="none"
									placeholder={'Type your custom subcategory'}
									placeholderTextColor= {'#CCC'} />
							</View>
						))}
					</KeyboardAvoidingView>
					<TouchableOpacity
						onPress={() => this.addCustomSubCategory(key, category)}
						disabled={foundCategory}>
						<Icon
							name={customSubcategories.size? "ios-remove-circle-outline" : "ios-add-circle-outline"}
							size={48}
							color='#333'
							style={{textAlign: 'center', marginTop: 15, opacity}} />
					</TouchableOpacity>
				</View>
			)
		})
	}

  render() {
    return (
			<View style={this.props.style}>
				<ScrollView
					automaticallyAdjustContentInsets={false}
					scrollEventThrottle={200}
					style={Styles.ftuxContainer}>
					{this.getListViews()}
				</ScrollView>
			</View>
    )
  }
}

const styles = StyleSheet.create({
	input: {
		flex: 1,
		flexDirection: 'row',
		marginLeft: 15,
		marginRight: 15,
		marginTop: 15,
		marginBottom: 15,
    color: '#333',
		fontSize: 18,
		height: 45,
		padding: 5,
    paddingLeft: 10,
    textAlign: 'left',
    backgroundColor: '#fff',
		borderWidth: 1,
		borderColor: '#DEDEDE'
	},
});
