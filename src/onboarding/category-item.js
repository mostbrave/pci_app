/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import { styles, constants } from '../styles'

import {
  Image,
  Switch,
  Text,
	TouchableHighlight,
  View
} from 'react-native'

export default class Category extends Component {
	state = {
		checked: false
	}

	onCheck = (value) => {
		this.setState({checked: value})
		this.props.onPress(value, this.props.item.key)
	}

  renderInfo() {
    return (
      <View style={this.props.containerStyle}>
        <Text style={this.props.helpStyle}>{this.props.item.desc}</Text>
      </View>
    )
  }

  render() {
    return (
      <View style={styles.li}>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'flex-start'}}>
          <Text style={styles.liText}>
            {this.props.item.title}
          </Text>
          <Switch
            style={{marginRight: 10}}
            onValueChange={this.onCheck}
            value={this.state.checked} />
        </View>

        {this.state.checked? this.renderInfo() : null}
      </View>
    )
  }
}
