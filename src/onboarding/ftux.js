import React, { Component, PropTypes } from 'react'
import Information from './information'
import SubCategories from './subcategories'
import Categories from './categories'
import Summary from './summary'
import {styles as Styles, theme_vars as Theme} from '../styles'
import { en } from '../constants'
import * as firebase from 'firebase'
import * as Progress from 'react-native-progress';

import {
  Navigator,
  StatusBar,
  StyleSheet,
  Text,
	TouchableHighlight,
  View
} from 'react-native'

export default class FTUX extends Component {
  state = {
    infoIsDisabled: true,
    categoriesIsDisabled: true,
    subcategoriesIsDisabled: true,
    schemaCategories: null,
    schemaSubCategories: null,
  }

	static propTypes = {
    onFinishFTUX: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    const database = firebase.database()
    database.ref('/categories').once('value').then(snapshot => {
      this.setState({schemaCategories: snapshot.val()})
    })

    database.ref('/subcategories').once('value').then(snapshot => {
      this.setState({schemaSubCategories: snapshot.val()})
    })
  }

  onInfoComplete = (isComplete, userData) => {
    this.setState({infoIsDisabled: !isComplete, userData})
  }

  onCategoriesComplete = (isComplete, categories) => {
    this.setState({categoriesIsDisabled: !isComplete, categories})
  }

  onSubCategoriesComplete = (isComplete, subcategories, customSubcategories) => {
    this.setState({subcategoriesIsDisabled: !isComplete, subcategories, customSubcategories})
  }

  onFinish = () => {
    const { userData, categories, subcategories, customSubcategories } = this.state
    this.props.onFinishFTUX(userData, subcategories, customSubcategories)
  }

  renderScene = (route, navigator) => {
    switch (route.name) {
      case 'categories':
        return (
          <Categories
            schemaCategories={this.state.schemaCategories}
            style={[Styles.ftuxContainer, Styles.containerNavMargin]}
            onComplete={this.onCategoriesComplete}/>
        )
      case 'information':
        return (
          <Information
            style={[Styles.ftuxContainer, Styles.containerNavMargin]}
            onComplete={this.onInfoComplete}/>
        )
      case 'subcategories':
				return (
          <SubCategories
            schemaCategories={this.state.schemaCategories}
            schemaSubCategories={this.state.schemaSubCategories}
            categories={this.state.categories}
            style={[Styles.ftuxContainer, Styles.containerNavMargin]}
            onComplete={this.onSubCategoriesComplete}/>
        )
			case 'summary':
				return (
          <Summary
            userData={this.state.userData}
            schemaCategories={this.state.schemaCategories}
            schemaSubCategories={this.state.schemaSubCategories}
            subcategories={this.state.subcategories}
            customSubcategories={this.state.customSubcategories}
            style={[Styles.ftuxContainer, Styles.containerNavMargin]} />
        )
    }
  }

	next = (route, navigator) => {
		switch (route.name) {
			case 'information':
				navigator.push(routes.categories)
				break;
			case 'categories':
				navigator.push(routes.subcategories)
				break;
			case 'subcategories':
				navigator.push(routes.summary)
				break;
			default:

		}
	}

  render() {
    if(!this.state.schemaCategories || !this.state.schemaSubCategories) {
      return <View></View>
    }

    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="white" barStyle="light-content"/>
        <Navigator
          initialRoute={routes.information}
          renderScene={this.renderScene}
          navigationBar={
            <Navigator.NavigationBar
              style={Styles.navigator}
              routeMapper={{
                LeftButton: (route, navigator, index, navState) => {
                  if(index > 0) {
                    return (
                      <TouchableHighlight
                        onPress={() => navigator.pop()}
                      >
                        <View style={styles.navBarLeft}>
                          <Text style={Styles.navigatorText}>{en.back}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  }

                  return null
                },
                RightButton: (route, navigator, index, navState) => {
                  if(route.name === 'summary') {
                    return (
                      <TouchableHighlight
                        onPress={this.onFinish}
                      >
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorText}>{en.finish}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  } else if(route.name === 'information') {
                    if(this.state.infoIsDisabled) {
                      return (
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorTextDisabled}>{en.next}</Text>
                        </View>
                      )
                    }

                    return (
                      <TouchableHighlight
                        onPress={() => this.next(route, navigator)}
                      >
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorText}>{en.next}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  } else if(route.name === 'categories') {
                    if(this.state.categoriesIsDisabled) {
                      return (
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorTextDisabled}>{en.next}</Text>
                        </View>
                      )
                    }

                    return (
                      <TouchableHighlight
                        onPress={() => this.next(route, navigator)}
                      >
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorText}>{en.next}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  } else if(route.name === 'subcategories') {
                    if(this.state.subcategoriesIsDisabled) {
                      return (
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorTextDisabled}>{en.next}</Text>
                        </View>
                      )
                    }

                    return (
                      <TouchableHighlight
                        onPress={() => this.next(route, navigator)}
                      >
                        <View style={styles.navBarRight}>
                          <Text style={Styles.navigatorText}>{en.next}</Text>
                        </View>
                      </TouchableHighlight>
                    )
                  }
                },
                Title: (route, navigator, index, navState) => {
                  return (
                  <View style={{alignItems: 'center'}}>
                  <View><Text style={[Styles.navigatorText,{paddingBottom:4}]}>{route.title}</Text></View>
                  <View><Progress.Bar progress={route.step} width={200} /></View>
                  </View>);
                }
             }}
           />
          }
        />
      </View>
    )
  }
}

const routes = {
  information: {
    name: "information",
    title: "User Information",
    step: 0.25
  },
	categories: {
    name: "categories",
    title: "Categories",
    step: 0.5
  },
  subcategories: {
    name: "subcategories",
    title: "SubCategories",
    step: 0.75
  },
  summary: {
    name: "summary",
    title: "Summary",
    step: 1
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
	},
	tab: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom: 10,
  },
  tabs: {
    height: 45,
    flexDirection: 'row',
    paddingTop: 5,
    borderWidth: 1,
    borderBottomWidth: 0,
    borderLeftWidth: 0,
    borderRightWidth: 0,
    borderTopColor: 'rgba(0,0,0,0.05)',
  },
  navigationBarText: {
    color: '#333333',
    fontSize: 14,
    flex: 1
  },
  navBarLeft: {
    marginLeft: 15
  },
  navBarRight: {
    marginRight: 15
  },
  navBarTextActions: {
    fontWeight: 'bold',
    fontSize: 14
  },
  navBarTextActionsDisabled: {
    fontWeight: 'bold',
    color: '#AAA',
    fontSize: 14
  },
})
