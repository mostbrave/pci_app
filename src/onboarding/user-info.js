import React, { Component } from 'react'
import {styles as Styles, InfoStyle} from '../styles'
import Icon from 'react-native-vector-icons/Ionicons'
import { docs } from '../schema.json'

import {
  Text,
  View,
  ScrollView,
} from 'react-native'

import {
	Form, Separator, InputField, LinkField,
  SwitchField, PickerField, DatePickerField
} from 'react-native-form-generator'

export default class UserInfo extends Component {
  constructor(props) {
    super(props)
    this.state = {
      formData: {},
      showTandC: false,
    }
  }

	handleFormChange = (data) => {
		this.props.onComplete(this.isFormComplete(data), data)
	}

  isFormComplete(data) {
    return data.first_name &&
			 data.last_name &&
		 	 data.gender &&
		 	 data.birthday &&
		   data.has_accepted_conditions &&
		 	 data.recovery_months
  }

	toggleTandC = () => {
		this.setState({showTandC: !this.state.showTandC})
	}

	renderTandC() {
		if(this.state.showTandC) {
			return (
        <ScrollView
          keyboardShouldPersistTaps="always"
          style={{backgroundColor: "#FFFFFF", paddingLeft:20,paddingRight:20, height:100}}>
            <Text style={{fontSize: 8, color: 'grey'}}>{docs.toc}</Text>
        </ScrollView>
			)
		}

		return null
	}

	render() {
		return (
      <ScrollView keyboardShouldPersistTaps="always" style={{height:200}}>
				<Form
					ref='registrationForm'
					onChange={this.handleFormChange}
					label="User Information">
					<InputField
						ref='first_name'
						placeholder='First Name'
						style={Styles.input}/>

					<InputField
						ref='last_name'
						placeholder='Last Name'
						style={Styles.input}/>

					<PickerField ref='gender'
						label='Gender'
						options={{
							'': '',
							male: 'Male',
							female: 'Female',
							other: 'Other'
						}}
						labelStyle={Styles.input}
						valueStyle={[Styles.input, Styles.inputValue, Styles.inputValueWicon]}
						style={{backgroundColor: 'blue', color: '#fff'}}
						iconRight={
							<Icon
  	            name='ios-list-outline'
  	            size={30}
  	            color='rgb(7,116,200)'
								style={{marginTop: 7, position:'absolute', right: 10}}/>
						}/>
					<DatePickerField ref='birthday'
						minimumDate={new Date('1/1/1950')}
						maximumDate={new Date(((new Date()).getFullYear() - 18)+'')}
						placeholder='Birthday'
						placeholderStyle={Styles.input}
						valueStyle={[Styles.input, Styles.inputValue, Styles.inputValueWicon]}
						mode='date'
						iconRight={
							<Icon
  	            name='ios-calendar-outline'
  	            size={24}
  	            color='rgb(7,116,200)'
								style={{marginTop: 0, position:'absolute', right: 0}}/>
						}/>

					<InputField
						ref='weight'
						placeholder='Weight (in pounds)'
						style={Styles.input}
						keyboardType='numeric'/>
					<InputField
						ref='recovery_months'
						placeholder='Months you have been in recovery'
						style={Styles.input}
						keyboardType='numeric'/>
					<LinkField
						label="Terms & Conditions"
						onPress={this.toggleTandC}
						labelStyle={[Styles.input, Styles.inputValue]}
						iconRight={
							<Icon
  	            name={this.state.showTandC? 'ios-arrow-down-outline' : 'ios-arrow-forward-outline'}
  	            size={24}
  	            color='rgb(7,116,200)'
								style={{marginTop: 10, position:'absolute', right: 10}}/>
						}/>
					{this.renderTandC()}
					<SwitchField label='I accept Terms & Conditions'
          	ref="has_accepted_conditions"
          	//helpText='Please read carefully the terms & conditions'
						labelStyle={[Styles.input, Styles.inputValue]}/>
				</Form>
      </ScrollView>
		)
	}
}
