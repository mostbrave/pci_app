/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import { styles } from '../styles'
import CheckBox from 'react-native-checkbox'
import { fromJS } from 'immutable'

import {
  Image,
  Text,
	TouchableHighlight,
  View
} from 'react-native'

export default class SummaryItem extends Component {
	constructor(props) {
		super(props)
	}

  render() {
    return (
      <View style={styles.li}>
        <View style={{flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <Text style={styles.liText}>
            {this.props.item.subcategory}
          </Text>
        </View>
      </View>
    )
  }
}
