/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import * as Exceptions from '../error-codes'
import Category from './category-item'
import { fromJS, List, Map, toJS } from 'immutable'
import { styles as Styles, CategoriesStyle, InfoBoxStyle } from '../styles'
import Icon from 'react-native-vector-icons/Ionicons'
import InfoBox from '../components/info-box'

import {
	ListView,
	ScrollView,
  StyleSheet,
  Text,
	TextInput,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Categories extends Component {
  constructor(props) {
    super(props)
		let rows = []
		const {schemaCategories} = this.props
		const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 })

		for(const key of Object.keys(schemaCategories)) {
			const value = schemaCategories[key]
			rows.push({
				key,
				title: value.text,
				desc: value.desc
			})
		}

		this.state = {
			errorMsg: '',
			dataSource: ds.cloneWithRows(rows),
			addedCategories: List(),
			helpIsHidden: false,
			limit: 7,
		}

  }

	closeHelp = () => {
		this.setState({helpIsHidden: true})
	}

	setCategory = (add, category) => {
		if(!add) {
			const newList = this.state.addedCategories.delete(
				this.getCategoryIndex(category)
			)

			this.setState({addedCategories: newList}, () => {
				this.props.onComplete(this.state.addedCategories.size == this.state.limit, this.state.addedCategories)
			})
		} else {
			this.setState({addedCategories: this.state.addedCategories.push(category)}, () => {
				this.props.onComplete(this.state.addedCategories.size == this.state.limit, this.state.addedCategories)
			})
		}
	}

	getCategoryIndex(category) {
		return this.state.addedCategories.findIndex(cat => {
			return cat === category
		})
	}

	renderCategory = (item) => {
		return (
			<Category
				containerStyle={Styles.helpContainerStyle}
				helpStyle={Styles.helpStyle}
				item={item}
				onPress={this.setCategory} />
		)
	}

	renderHelp() {
		return (
			<InfoBox
				onClose={this.closeHelp}
				containerStyle={{backgroundColor: 'rgba(0,0,0,0.7)'}}
				type='info'
				iconColor='#FFF'>
				<Text style={{color: '#FFF'}}>
					Please select {this.state.limit} categories that you want to track. Once you select categories,
					you will be asked to select one or more sub-categories in the next scene.
				</Text>
			</InfoBox>
		)
	}

  render() {
		return (
			<View style={this.props.style}>
				<Counter start={this.state.addedCategories.size}
				end={this.state.limit} />
				{this.renderHelp()}
				<ScrollView
					automaticallyAdjustContentInsets={false}
					scrollEventThrottle={200}
					style={Styles.ftuxContainer}>

					<ListView
						initialListSize ={1}
						dataSource={this.state.dataSource}
						renderRow={this.renderCategory} />
				</ScrollView>
			</View>
    )
  }
}

class Counter extends Component {
	shouldComponentUpdate(nextProps, nextState) {
		return this.props.start !== nextProps.start
	}

	render() {
		return (
			<View style={CategoriesStyle.countContainer}>
				<Text style={CategoriesStyle.count}>{this.props.start} of {this.props.end} selected</Text>
			</View>
		)
	}
}
