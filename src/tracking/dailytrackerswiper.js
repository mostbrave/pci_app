'use strict';

import React, {Component, PropTypes } from 'react';
import {StyleSheet, Text, View, Image,TouchableOpacity} from 'react-native';
import firebase from 'firebase';
import * as WaitingProgress from '../generic/progress';
import Iconz from 'react-native-vector-icons/Ionicons';

import {styles as Styles, main,
  theme_vars, theme} from '../styles';
import * as helpers from '../generic/helpers';
import Score from '../calculator/score';
import Subcat from '../category/subcat';
import * as corrector from './scorekeeper';
import SomethingWrong from '../generic/somethingwrong';
import SwipeCards from './SwipeCards';
import * as catutil from '../category/categoryhelper';
import Card from './card';
import NoMoreCards from './nocards';
import {MenuButtonGenerator} from '../generic/helpers';

export default class Swiper extends Component {
  static propTypes = {
    menuHandler: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    var todaysDate = helpers.getTodaysDateinMilliseconds();

    //If user had some inactivity, fill the gap
    //Add to the DB all the missing dates with default scores
    corrector.closeGap();

    //pass them to the class state object
    this.state =  {
      todaysDate: todaysDate,
      isLoading: 1,
      errorLoading: false,
      cards: new Array(),
    }
  }

  componentDidMount = function() {

var cards= new Array();

        catutil.getFullSubcatInfo(function(subcatArray){
                //This check is to make sure the user did not have an error
                //while setting up the categories during the onboarding process
                if(Object.keys(subcatArray).length > 0){

              //check if the user has rated any throughout the day
              //not to repeat it again. If the user completed rating
              //all subcats from cards so the user does not get to
              //all subcats, then this section should remove
              //rate the subcats again. Let's say the user rated 2
              //subcats and exited the app, then logged in again the same
              //day, the user should only see the ones that are incomplete
              firebase.database().ref('/tracking/' +
                  firebase.auth().currentUser.uid + '/dailyscores/' +
                  this.state.todaysDate + '/').once('value').then(
                    function(alreadyRatedCards) {
                    //If the user completed the rating activity of the day, then
                    //Skip retrieving any of the cards
                    if(alreadyRatedCards &&
                    Object.keys(subcatArray).length===alreadyRatedCards.numChildren()){
                      this.setState({
                          isLoading: this.state.isLoading + 1,
                      });
                      return;
                    }else{

                  //This will hold values of all subcat keys that the user already rated
                  //Example of the values would be: e.g. ['subcat1', 'subcat32', 'subcat16',...]
                  var alreadyRated = [];
                  alreadyRatedCards.forEach(function(objVal) {
                      alreadyRated.push(objVal.key);
                  });
                  //This section populates the cards that the user rates
                  // userSubcatIDs.forEach(function(userSubcatID) {
                  Object.keys(subcatArray).forEach(function(userSubcatID) {

                      //This array holds all the cards the user sees to rate
                      //do not add the subcat to the card list if user rated already
                      if (!alreadyRated.includes(userSubcatID)) {
                              //retrieve the number of all time thumbs up and
                              //down
                                      cards.push(subcatArray[userSubcatID]);
                                      //Once all the values have been inserted
                                      //exit the loading mode
                                      this.setState({
                                        cards: cards,
                                      });
                                      if(cards.length==
                                        (Object.keys(subcatArray).length -
                                         alreadyRatedCards.numChildren())){
                                        this.setState({
                                            isLoading: this.state.isLoading + 1,
                                        });
                                      }

                      }
                  }.bind(this));
                }
              }.bind(this));
            }else{
                this.setState({ errorLoading: true });
            }
    }.bind(this));
  }//End of method


  //This method is responsible for updating the user scores in DB
  updateScore(score){
    firebase.database().ref('/tracking/'+
    firebase.auth().currentUser.uid + '/dailyscores/' +
    this.state.todaysDate + '/').update(score);
  }

  handleYup (card) {
    //put the score before it gets persisted in the
    //format of subcat1 : 1
    var score = {};
    score[card.subcatID]=Score.getHandleYupValue();

    //Update the database with the user rating
    this.updateScore(score);
    //update all time total of yes count for this subcatID
    corrector._incrementAggregateCount('yes' , card.subcatID);

  }
  handleNope (card) {
    //put the score before it gets persisted in the
    //format of subcat1 : 1
    var score = {};
    score[card.subcatID]=Score.getHandleNopeValue() ;

    //Update the database with the user rating
    this.updateScore(score);
    //update all time total of no count for this subcatID
    corrector._incrementAggregateCount('no' , card.subcatID);

  }
  yup = function() {
    this.refs['swiper']._goToNextCard('yes')
  }

  nope = function(){
    this.refs['swiper']._goToNextCard('no')
  }

  render() {
    const menuButton=<MenuButtonGenerator
    menuHandler={this.props.menuHandler}
    menuTitle='Daily Tracker'/>;
      if(this.state.errorLoading){
        return (
          <View style={{flex: 1, flexDirection: 'column',
          backgroundColor: theme_vars[theme].backgroundColor}}>
              {menuButton}
            <View style={{flex: 10, marginBottom: 30,
              marginLeft:10, marginRight: 10}}>
              <SomethingWrong />
            </View>
          </View>

        );
      }else{
      if(this.state.isLoading < 2){
              return (
                <View style={styles.container}>
                  <WaitingProgress.ProgressCircle />
                </View>);
      }else{

        return (
          <View style={{flex: 1, flexDirection: 'column',
          backgroundColor: theme_vars[theme].backgroundColor}}>
              {menuButton}
            <View style={{flex: 10, marginBottom: 30,
              marginLeft:10, marginRight: 10}}>
          <SwipeCards
            ref = {'swiper'}
            cards={this.state.cards}
            loop={false}

            renderCard={(cardData) =>
              <Card {...cardData} handleYup={this.yup.bind(this)}
                    handleNope={this.nope.bind(this)} />}
            renderNoMoreCards={() => <NoMoreCards menuButton={menuButton}/>}
            showYup={true}
            showNope={true}

            handleYup={this.handleYup.bind(this)}
            handleNope={this.handleNope.bind(this)}
          />
          </View>


          </View>
        );
      }
    }
  }//End of Render Method
}//End of Class

const styles = StyleSheet.create({
  card: {
    backgroundColor: 'grey',
    borderColor: 'black',
    borderWidth: 2
  },
  subcat: {
    justifyContent: 'center',
    alignItems: 'center'
  },
  subcatsummary: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    padding: 1
  },
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 20,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  circles: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  progress: {
    margin: 10,
  },
  buttons:{
    width:80,
    height:80,
    borderWidth:10,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:40
  },
  swipericons:{
    width:94,
    height:94,
    borderWidth:4,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:40
  },
  buttonSmall:{
    width:50,
    height:50,
    borderWidth:10,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:25
  }
})
