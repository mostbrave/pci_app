'use strict';

import React, {Component} from 'react';
import ReactNative from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles as Styles, main} from '../styles';
import DailyTrackerSwiper from './dailytrackerswiper.js';
import {MenuButtonGenerator} from '../generic/helpers';

const styles = require('../styles.js');
const {StyleSheet, View, Text} = ReactNative;

export default class DailyTracker extends Component {



  render() {
    const menuButton=<MenuButtonGenerator
    menuHandler={this.props.menuHandler}
    menuTitle='Account'/>;


      return (
        // Try removing the `flex: 1` on the parent View.
        // The parent will not have dimensions, so the children can't expand.
        // What if you add `height: 300` instead of `flex: 1`?
        <View style={{flex: 1, backgroundColor: 'grey'}}>
        {menuButton}


        </View>
      );
    }
}
