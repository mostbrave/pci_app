'use strict';

import React, {Component} from 'react';
import {Text, View, Image} from 'react-native';
import Illustration from '../generic/illustration';
import {theme, theme_vars} from '../styles';
let NoMoreCards = React.createClass({
  render() {
    return (
      <View style={{flex: 5}}>

        <View style={{flex: 3,
          backgroundColor: theme_vars[theme].backgroundColor,
      alignItems: 'center', justifyContent: 'center'}}>
      <Illustration
      image={require('../../assets/completed_rating.png')}
      largeText={'You have completed rating all your activies for the day!'}
      smallText={'You will be able to rate your progress again on the next day.'} />
        </View>
      </View>
    )
  }
});

export default NoMoreCards;
