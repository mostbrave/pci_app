'use strict';

import React, {Component, PropTypes } from 'react';
import {StyleSheet, Text, View, Image,TouchableOpacity} from 'react-native';
import Iconz from 'react-native-vector-icons/Ionicons';
import {styles as Styles, main, theme, theme_vars} from '../styles';

let Card = React.createClass({
  render() {
    console.log(this.props.subcatID);
    console.log(this.props.category.categoryID);

    return (
      <TouchableOpacity style={Styles.container} activeOpacity={0.7}>
      <View style={{flex: 1, padding: 10, margin: 10
        , borderRadius:10
      }}>
          <View style={{ flex: 0.5,
          overflow: 'hidden', borderRadius: 9, alignItems: 'center' }}>
              <Image
              source={getImage(this.props.category.categoryID)}
              style={styles.swipericons}/>
          </View>

          <View style={{ flex: 0.5, alignItems: 'flex-start'}}>
              <View>
                  <View>
                      <View style={{padding: 10}}>
                          <Text style={{fontSize: 28,
                            color: theme_vars[theme].activeTextColor}}>
                            {this.props.category.categoryName}
                          </Text>
                      </View>
                      <View style={{padding: 6}}>
                          <Text style={{fontSize: 20,
                            color: theme_vars[theme].activeTextColor}}>
                            {this.props.subcatName}
                          </Text>
                      </View>
                  </View>
              </View>
          </View>

          <View style={{alignItems:'flex-end', flexDirection: 'row'}}>
            <View style={{padding:13,  borderLeftWidth:1,borderColor:'#e3e3e3',
            alignItems:'center', justifyContent:'space-between'}}>
              <Iconz name='ios-thumbs-down' size={20}
              color={theme_vars[theme].activeTextColor}
              style={{}} />
              <Text style={{fontSize:16, fontWeight:'200', color:'#555'}}>
              {this.props.thumbsDownCount}
              </Text>
            </View>
            <View style={{padding:13, borderLeftWidth:1,borderColor:'#e3e3e3',
            alignItems:'center', justifyContent:'space-between'}}>
              <Iconz name='ios-thumbs-up' size={20}
              color={theme_vars[theme].activeTextColor} />
              <Text style={{fontSize:16, fontWeight:'200', color:'#555'}}>
              {this.props.thumbsUpCount}
              </Text>
            </View>
          </View>

          <View style={{flexDirection:'row', alignItems:'center',
          justifyContent:'space-between'}}>
            <TouchableOpacity style = {styles.buttons} onPress = {() => this.props.handleNope()}>
              <View>
              <Iconz name='ios-thumbs-down' size={45} color="#f00" style={{backgroundColor: 'rgba(0,0,0,0)'}} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity style = {styles.buttons} onPress = {() => this.props.handleYup()}>
              <View>
                <Iconz name='md-heart' size={36} color="#33FF68" style={{marginTop:5,backgroundColor: 'rgba(0,0,0,0)'}} />
              </View>
            </TouchableOpacity>
          </View>

      </View>
</TouchableOpacity>
    )
  }
});

//This method retrieves the image based on category name
//These are static reference, and not retrieved automatically
//based on dynamically defined variables
var getImage = function(categoryID){
  if(categoryID){
    if(categoryID==='category1'){
      return require('../../assets/category1.png');
    }
    if(categoryID==='category2'){
      return require('../../assets/category2.png');
    }
    if(categoryID==='category3'){
      return require('../../assets/category3.png');
    }
    if(categoryID==='category4'){
      return require('../../assets/category4.png');
    }
    if(categoryID==='category5'){
      return require('../../assets/category5.png');
    }
    if(categoryID==='category6'){
      return require('../../assets/category6.png');
    }
    if(categoryID==='category7'){
      return require('../../assets/category7.png');
    }
    if(categoryID==='category8'){
      return require('../../assets/category8.png');
    }
    if(categoryID==='category9'){
      return require('../../assets/category9.png');
    }
    if(categoryID==='category10'){
      return require('../../assets/category10.png');
    }
    if(categoryID==='category11'){
      return require('../../assets/category11.png');
    }
    if(categoryID==='category12'){
      return require('../../assets/category12.png');
    }
  }
  //if no condition is executed, return default
  return require('../../assets/pci_default.png');
}

const styles = StyleSheet.create({

  buttons:{
    width:80,
    height:80,
    borderWidth:10,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:40
  },
  swipericons:{
    width:260,
    height:150,
    borderWidth:4,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:40,
    resizeMode: 'contain'
  },
  buttonSmall:{
    width:50,
    height:50,
    borderWidth:10,
    borderColor:'#e7e7e7',
    justifyContent:'center',
    alignItems:'center',
    margin: 3,
    borderRadius:25
  }
});

export default Card;
