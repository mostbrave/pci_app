'use strict';

import firebase from 'firebase';
import * as helpers from '../generic/helpers';
import Score from '../calculator/score'

/**This method corrects any gaps in user ratings because sometimes users
 * might skip a day or more.
 * The method takes a reference date as a starting point
 * It starts by looking at the day before the reference date
 * if it does not find the scores of the previous day, this
 * means the user skipped rating the previous day, it populates
 * it in the database with the default scores, and goes on checking
 * the 2nd day before, and so on, it will exist, once it finds scores
 * for that particular date in the database. This method will ultimately
 * ensure users do not have gaps in their scores and reports.
 * @Note -> This method will ensure the aggregate scores get maintained
 * should passed dates get updated
 * @param callback function to figure out gap and close it
 * format
 */
export var closeGap = function() {
    try {
        var ref = firebase.database().ref('/tracking/' +
            firebase.auth().currentUser.uid + '/dailyscores/');

        var lastDate = ref.limitToLast(1);
        //This retrieves the timestamp of the last recorded
        //user rating activity
        lastDate.once('child_added', function(snapshot) {
            //if the difference is more than a day, then the user
            //has definitely skipped at least a day. THis will getTime
            //recorded in the snapshot.key in ms something like 1477206000000
            console.log('Current Time ' + helpers.getTodaysDateinMilliseconds());
            console.log('SnApShote ' + snapshot.key);
            var timeDiff = helpers.getTodaysDateinMilliseconds() -
                snapshot.key;

            console.log('Time Diff: ' + timeDiff);
            var gapSize = Math.floor(timeDiff / (24 * 60 * 60 * 1000));
            console.log('Gap Size: ' + gapSize);
            //if difference is more than a day, then apparently
            //there is a gap
            constructAndFillGap(snapshot.key, gapSize);


        });
    } catch (ex) {
        console.error(ex.message);
        return false;
    }
}


//This method takes the last date a user has rated
//his progress, and find out if the user completed
//rating all his subcategories. If the user did not
//finish rating all his subcats during a particular
//day, this method should detect and fill the gap
//for that particular day only
function _FillPartialRatingGap(lastRatedDate, allUserSubcats) {
    var dayGap = {
        'mode': 'partial'
    };
    //Make sure the allUserSubcats array is valid and
    //properly populated
    if (!allUserSubcats && allUserSubcats.length < 1) {
        return false;
    }

    var ref = firebase.database().ref('/tracking/' +
        firebase.auth().currentUser.uid + '/dailyscores/' + lastRatedDate);

    ref.once('value').then(function(ratings) {
        console.log('Last known Date: ' + lastRatedDate);
        ratings.forEach(function(value) {
            console.log('Displaying historical data: ' + value.key);
            //remove items from the default list of subcats
            //whatever is left would be the day gap the user
            //missed to rated
            var i = allUserSubcats.indexOf(value.key);
            if (i != -1) {
                allUserSubcats.splice(i, 1);
            }

        });
        allUserSubcats.forEach(function(subcat) {
            console.log('Remaining Sucats: ' + subcat);
            dayGap[subcat] = Score.getHandleNopeValue();
        });
        //Insert the partial missing subcat rating
        //from the last rated day into Database
        if (allUserSubcats.length > 0) {
            console.log('Updating Gap with ' + allUserSubcats.length + ' missing items');
            firebase.database().ref('/tracking/' +
                firebase.auth().currentUser.uid + '/dailyscores/' +
                lastRatedDate + '/').update(dayGap);
        }
    });
}

/**
 * This method is an asynchronous method
 * that is used to retrieve all the subcategories
 * assigned to the logged in user.
 * @return array of strings representing subcat IDs
 */
var constructAndFillGap = function(startingTime, gapSize) {
    var dayScoreJSON = {};
    var gapTree = {};
    var userSubCats = [];

    //Make sure the start time is valid and the gap is valid which should check
    //that the user has skipped a day, and the start time is at least in 2016
    //this should validate that the time in milliseconds is valid
    if (!startingTime || !gapSize) {
        return false;
    }

    try {
        //This will read the data once with no callback events
        //when the database data is changed
        firebase.database().ref('users/' + firebase.auth().currentUser.uid +
            '/subcategories/').once('value').then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                //store all user subcats in array
                dayScoreJSON[childSnapshot.key] = Score.getHandleNopeValue();
                userSubCats.push(childSnapshot.key);
            });

            //check last rated date, and ensure user completed
            //rating all his activities/subcatergories for that day
            try {
                _FillPartialRatingGap(startingTime, userSubCats);
            } catch (e) {
                console.log(e.message);
            }

            //This section takes care of populating any missing days
            //users skip or do not rate their progress
            if (gapSize >= 2) {
                console.log('Filling Skipped Full Days Gap');
                dayScoreJSON['mode'] = 'skipped';
                console.log('startingTime: ' + startingTime);
                var parentDate = new Date();
                parentDate.setTime(startingTime);
                console.log(parentDate);
                for (var i = 0; i < gapSize - 1; i++) {
                    parentDate.setDate(parentDate.getDate() + 1);
                    parentDate.setHours(0, 0, 0, 0);
                    gapTree[parentDate.getTime()] = dayScoreJSON;
                }
                //check to mamke sure the array is not empty
                //before inserting into database
                if (gapTree && Object.keys(gapTree).length > 0) {
                    firebase.database().ref('tracking/' +
                        firebase.auth().currentUser.uid +
                        '/dailyscores/').update(gapTree);
                }
                console.log(gapTree);
            }
        });
    } catch (ex) {
        console.error(ex.message);
        return false;
    }
};

/** Everytime a user swipes right/left, this is triggered
* to keep track of the all time total of yes/no swipes
* @param subcatID as an argument
* @param mode with values of yes or no
* @return false if the argument passed are not valid
**/
export function _incrementAggregateCount(mode, subcatID) {
    //validate to ensure the subcatID is valid
    if (!subcatID || !mode || (mode!=='yes' && mode!=='no')) {
        return false;
    } else {
      console.log('mode ' + mode);
        firebase.database().ref('tracking/' +
            firebase.auth().currentUser.uid +
            '/aggregates/' +
              subcatID + '/' +
              mode + '/').once('value').then(function(snapshot) {
                var increment = 1;
                //ensure the total is not null, otherwise, it will be the very
                //first time building aggregate for user
                if (snapshot.val() && snapshot.val() > 0) {
                    increment += snapshot.val();
                }
                var updateValue = {};
                updateValue[mode] = increment;

                firebase.database().ref('tracking/' +
                    firebase.auth().currentUser.uid +
                    '/aggregates/'+ subcatID + '/').update(updateValue);
            });
    }
}
