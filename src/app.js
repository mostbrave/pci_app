import React, { Component } from 'react'
import * as config from '../app.config'
import MainScene from './main-scene'
import Authentication from './auth/auth'
import Registration from './auth/registration'
import ResetPassword from './auth/reset'
import Onboarding from './onboarding/onboarding'
import FTUX from './onboarding/ftux'
import * as firebase from 'firebase'
import {en, routes} from './constants'
import {styles as Styles, main} from './styles'
import Dashboard from './dashboard/dashboard'
import DailyTracker from './tracking/dailytrackerswiper'
import Reports from './reports/reports';
import DashboardHelp from './help/dashboardHelp';
import {Settings} from './settings/settings';

import {
  StyleSheet,
  Text,
  View,
  Navigator,
  TouchableHighlight,
  Image
} from 'react-native'

export default class App extends Component {
  state = {
    currentUser: null,
    hideNavigationBar: false,
    menuOpen: true,
  }

  constructor(props) {
    super(props)
    firebase.initializeApp(config.firebase);
    firebase.auth().onAuthStateChanged(this.initSession);
  }

  menuHandler = (open=null) => {
    this.setState({
      menuOpen: open || !this.state.menuOpen
    })
  }

  /**
   * Initializes session for current user or new user.
   **/
  initSession = (user) => {
    console.log('\n\n\n\n\nChecking for session\n\n\n\n');
    if(user) {
      this.setState({currentUser: user, hideNavigationBar: true})

      firebase.database().ref('/users/' + user.uid).once('value').then((snapshot) => {
        const userObj = snapshot.val()

        if(!userObj) {
          firebase.database().ref('/users/' + user.uid).set({
            username: user.email
          })

          this.getOnboardingRoute()
        } else if(userObj && !userObj.ftux) {
          this.getOnboardingRoute()
        } else {
          this.getMainRoute()
        }
      })

    } else {
      this.setState({hideNavigationBar: false})
      this.getLoginRoute()
    }
  }

  getOnboardingRoute() {
    this.refs['nav'].resetTo(routes.onboarding)
  }

  getLoginRoute() {
    this.refs['nav'].resetTo(routes.login)
  }

  getMainRoute() {
    this.refs['nav'].resetTo(routes.main)
  }

  getFTUXScene = ({push}) => {
    push(routes.ftux) //FTUX Screen
  }

  getRegistrationScreen = ({push}) => {
    push(routes.register) //Registration Screen
  }

  getResetScreen = ({push}) => {
    push(routes.reset) //Registration Screen
  }

  getMainScene = ({push}, user, subcategories, customSubcategories) => {
    //1. push user information to database
    const userSession = firebase.auth().currentUser

    let rows = {}
    let index = 0

    subcategories.map(subcat => {
      rows['subcategory'+index] = {
        category: subcat.category,
        name: subcat.name
      }
      index++
		})

		customSubcategories.map(custom => {
      rows['subcategory'+index] = {
        category: custom.getIn(['category', 'key']),
        name: custom.get('value')
      }
      index++
		})

    firebase.database().ref('/users/' + userSession.uid).update({
      first_name: user.first_name,
      last_name: user.last_name,
      birthday: user.birthday,
      gender: user.gender,
      weight: user.weight,
      recovery_months: user.recovery_months,
      ftux: true,
      subcategories: rows,
    }).then(() => {
      userSession.updateProfile({
        displayName: user.first_name,
      }).then(function() {
        // Update successful.
          push(routes.main) //Main Screen
      }, function(error) {
        // An error happened.
          push(routes.main) //Main Screen
      });

    })
  }

  renderScene = (route, navigator) => {
    switch (route.name) {
      case 'initialization':
        return (
          <View style={styles.container}>
            <Image source={require('../assets/splash.png')} style={main.splash} />
          </View>
        )
      case 'dashboard':
        return <Dashboard navigator={navigator} menuHandler={this.menuHandler} />
      case 'reports':
        return <Reports navigator={navigator} menuHandler={this.menuHandler} />
      case 'account':
        return <Settings navigator={navigator} menuHandler={this.menuHandler} />
      case 'tracker':
        return <DailyTracker navigator={navigator} menuHandler={this.menuHandler} />
      case 'register':
        return <Registration navigator={navigator} />
      case 'reset':
        return <ResetPassword navigator={navigator} />
      case 'login':
        return (
          <Authentication
            onRegister={() => this.getRegistrationScreen(navigator)}
            onReset={() => this.getResetScreen(navigator)}/>
        )
      case 'onboarding':
        return <Onboarding onFinishOnboarding={() => this.getFTUXScene(navigator)} />
      case 'ftux':
        return <FTUX onFinishFTUX={(user, cat, sub_cat) => this.getMainScene(navigator, user, cat, sub_cat)}/>
        case 'dashboardHelp':
          return <DashboardHelp navigator={navigator} menuHandler={this.menuHandler} />
        case 'reportsHelp':
          return <ReportsHelp navigator={navigator} menuHandler={this.menuHandler} />
        case 'accountHelp':
          return <SettingsHelp navigator={navigator} menuHandler={this.menuHandler} />
        case 'trackerHelp':
          return <DailyTrackerHelp navigator={navigator} menuHandler={this.menuHandler} />
      default:
        return (
          <MainScene
            menuOpen={this.state.menuOpen}
            menuHandler={this.menuHandler}
            navigator={navigator}
            user={this.state.currentUser} />
        )
    }
  }

  render() {
    if(!this.state.hideNavigationBar) {
      return (
        <Navigator
          ref="nav"
          initialRoute={routes.init}
          renderScene={this.renderScene}
          navigationBar={
            <Navigator.NavigationBar
              style={Styles.navigator}
              routeMapper={{
                LeftButton: (route, navigator, index, navState) => {
                  if(route.showLeftNav) {
                    return (
                      <TouchableHighlight
                        onPress={navigator.pop}
                        style={styles.navBarLeft}>
                        <Text style={styles.navBarTextActions}>Cancel</Text>
                      </TouchableHighlight>
                    )
                  }

                  return null
                },
               RightButton: (route, navigator, index, navState) => { return null },
               Title: (route, navigator, index, navState) => {
                 if(route.title) {
                   return <Text style={Styles.navigatorText}>{route.title}</Text>
                 }

                 return null
               }
             }}
           />
          }
        />
      )
    } else {
      return (
        <Navigator
          ref="nav"
          initialRoute={routes.init}
          renderScene={this.renderScene}/>
      )
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  navigationBar: {
    backgroundColor: '#111'
  },
  navigationBarText: {
    marginTop: 15,
    color: '#333333',
    fontSize: 12
  },
  navBarLeft: {
    marginTop: 15,
    marginLeft: 15
  },
  navBarTextActions: {
    fontWeight: 'bold',
    fontSize: 12,
    color: '#ffffff'
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center', alignItems: 'center', resizeMode: 'cover',
    width: null, height: null
  },
  init: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
