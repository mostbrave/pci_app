import React, { Component, PropTypes } from 'react'
import { signout, updatePassword } from '../auth/auth-service'
import * as Exceptions from '../error-codes'
import UpdateForm from './update-form'
import resetAllUserTrackingInformation from '../profile/reset'
import * as Progress from 'react-native-progress'
import Immutable from 'immutable'
import Icon from 'react-native-vector-icons/Ionicons'
import {styles as Styles, main, theme_vars, theme} from '../styles'
import {routes} from '../constants'
import ResetDetails from '../auth/reset-details'
import Reset from '../auth/reset'

import {
  Keyboard,
  ListView,
  RecyclerViewBackedScrollView,
  StatusBar,
  StyleSheet,
  Text,
	TouchableHighlight,
  TouchableWithoutFeedback,
  View,
  Alert,
  Modal
} from 'react-native'

import {MenuButtonGenerator} from '../generic/helpers';

const ResetDetailsModal = ({visible}) => (
  <Modal
    animationType="slide"
    transparent={false}
    visible={visible}>
   <Reset />
  </Modal>
)

class Settings extends Component {
  static propTypes = {
    menuHandler: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

    this.state = {
      datasource: ds.cloneWithRows(['Update Password', 'Reset Profile', 'Sign Out']),
      isLoading: false,
      visible: false,
    }
  }

  getIcon(rowID){
    if(typeof(rowID)!="undefined"){
      if(rowID==0){
        return <Icon name='md-lock' size={40} color="orange" style={{}} />;
      }else if (rowID==1) {
        return <Icon name='md-refresh' size={40} color="#ff0000" style={{}} />
      }else if (rowID==2) {
        return <Icon name='md-log-out' size={40} color="#00FF00" style={{}} />
      }
    }
  }

  resetAccount = async () => {
    this.state.isLoading=true
    await resetAllUserTrackingInformation()
    this.state.isLoading=false
    this.props.navigator.push(routes.ftux)
  }

  toggleResetDetailsModal = () => {
    console.log({visible: this.state.visible});
    this.setState({visible: !this.state.visible})
  }

  renderRow = (
    rowData,
    sectionID,
    rowID,
    highlightRow,
    handleModalOpen,
  ) => {
    return (
      <TouchableHighlight
        key={sectionID}
        onPress={() => {
          highlightRow(sectionID, rowID)

          if(rowID == 0) {
            this.toggleResetDetailsModal()
          } else if(rowID == 2) { //Signout
            signout()
          } else if(rowID == 1) { //Update

                    Alert.alert(
                        'Reset Profile',
                        'Are you sure you want to reset your profile.' +
                        ' This will delete all your data!!!', [
                            {
                                text: 'Cancel',
                                onPress: () => console.log('Cancel Pressed'),
                                style: 'cancel'
                            },
                            {
                                text: 'Continue',
                                onPress: this.resetAccount
                            },
                        ], {
                            cancelable: false
                        }
                    );


            //let route = Immutable.fromJS(this.props.route)
            //route = route.set('scene', 'update_password')
            //this.props.navigator.push(route.toJSON())
          }
        }}
      >


        <View style={styles.row}>
            <View>
                {this.getIcon(rowID)}
            </View>
            <View style={styles.row}>
              <Text style={styles.text}>
                {rowData}
              </Text>
            </View>
        </View>
      </TouchableHighlight>
    )
  }

  renderSeparator = (sectionID: number, rowID: number, adjacentRowHighlighted: bool) => {
    return (
      <View
        key={`${sectionID}-${rowID}`}
        style={{
          height: 1,
          backgroundColor: '#CCCCCC',
        }}
      />
    )
  }

  render() {
    const { isLoading, visible } = this.state

    if(!isLoading) {
      const menuButton=<MenuButtonGenerator
      menuHandler={this.props.menuHandler}
      menuTitle='Account'/>;
      return (
        <View style={{flex:1}}>
          {menuButton}
          <ResetDetailsModal visible={visible} />
          <View style={Styles.container}>
            <ListView
              dataSource={this.state.datasource}
              renderRow={this.renderRow}
              renderScrollComponent={props => <RecyclerViewBackedScrollView {...props} />}
              renderSeparator={this.renderSeparator}
              style={main.content} />
    			</View>
        </View>
      )
    } else {
      return (
        <View style={styles.container}>
        <Progress.CircleSnail style={styles.progress}
        color={[ 'green','blue', 'red']} />
        </View>);
    }
  }
}

class UpdatePassword extends Component {
  constructor(props) {
    super(props)

    this.state = {
      errorMsg: ''
    }
  }

  update(password) {
		updatePassword(password)
			.then(() => {
        console.log('success');
				signout()
			})
			.catch((err, code) => {
        console.log('error: ' + code);
	  		this.showErrorMsg(err.code)
			})
  }

  onUpdate = (password) => {
    this.showErrorMsg()
		this.update(password)
	}

  showErrorMsg(error) {
    this.setState({errorMsg: error? Exceptions.code[error]: ''})
  }

  render() {
    return (
			<TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
				<View style={[this.props.style, styles.updateContainer]}>
					<StatusBar
						barStyle="default"
					/>

					<UpdateForm style={styles.form} onUpdate={this.onUpdate} />

					<Text style={styles.errorTxt}>
						{this.state.errorMsg}
					</Text>
				</View>
			</TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    padding: 10,
    backgroundColor: theme_vars[theme].backgroundColor,
  },
  text: {
    flex: 2,
    fontSize: 16,
    color: theme_vars[theme].activeTextColor
  },
  form: {
		marginTop: 150,
    flex: 2,
		padding: 0
  },
  errorTxt: {
    flex: 1,
    color: '#ff0000'
  },
  updateContainer: {
    alignItems: 'center'
	}
})

export {
  Settings,
  UpdatePassword
}
