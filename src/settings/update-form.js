/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
  View
} from 'react-native'

export default class UpdateForm extends Component {
  static propTypes = {
    onUpdate: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      password: ''
    }
  }

  onNext = (ref) => {
    this.refs[ref].focus()
  }

  render() {
    return (
			<View style={this.props.style}>
				<View style={styles.inputWrapper} >
					<TextInput
            ref="pwd"
						style={styles.input}
						placeholder={'Password'}
            placeholderTextColor= {'#333333'}
						secureTextEntry={true}
            value={this.state.password}
            onChangeText={password => this.setState({password})}
            onSubmitEditing={() => this.props.onUpdate(this.state.password)}
					/>
				</View>
				<TouchableHighlight
          ref="submit"
					onPress={() => this.props.onUpdate(this.state.password)}
					style={styles.button}>
					<Text style={styles.buttonText}>Update</Text>
				</TouchableHighlight>
			</View>
    );
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
		width: 300,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderBottomColor: '#DEDEDE'
	},
	input: {
    color: '#333333',
		fontSize: 11,
		height: 45,
		padding: 5,
    paddingLeft: 10,
    textAlign: 'left'
	},
	button: {
		width: 75,
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#0774FA',
    borderRadius: 3,
		marginTop: 25
	},
	buttonText: {
		color: '#FFF',
    fontSize: 11
	}
});
