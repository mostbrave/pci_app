import React from 'react';
import firebase from 'firebase';
import {moveNode, getCurrentTimeinMilliseconds} from '../generic/helpers';

const resetAllUserTrackingInformation = () => {
  const archiveDate = getCurrentTimeinMilliseconds()
  //Move tracking information
  moveNode('/tracking/' + firebase.auth().currentUser.uid,
  '/archive/' + firebase.auth().currentUser.uid + '_' + archiveDate)

  //Move user subcategories information
  moveNode('/users/' + firebase.auth().currentUser.uid + '/subcategories',
  '/archive/' + firebase.auth().currentUser.uid + '_' + archiveDate +
  '/subcategories/')

  //remove the ftux flag
  return firebase.database().ref('/users/' + firebase.auth().currentUser.uid + '/ftux').remove()
}

export default resetAllUserTrackingInformation
