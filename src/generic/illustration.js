'use strict';

  import React from 'react';
  import { View, Text, StyleSheet, Image } from 'react-native';
  import * as Progress from 'react-native-progress';
  import {theme, theme_vars} from '../styles';
  const styles = StyleSheet.create({
    regularText:{
      fontSize: 18,
      color: theme_vars[theme].activeTextColor
    },
    smallText:{
      fontSize: 14,
      color: theme_vars[theme].activeTextColor
    }
  });


  const Illustration = (props) => (
    <View style={{alignItems: 'center',
  justifyContent: 'center'}}>
    <View>
      <Image
      style={{width: props.width?props.width:200, height: props.height?props.height:200}}
      source={props.image}/>
      </View>
      <View style={{margin: 40, alignItems: 'flex-start',
    justifyContent: 'center'}}>
      <Text style={styles.regularText}>
        {props.largeText}
      </Text>
      <Text style={styles.smallText}>
        {props.smallText}
      </Text>
      </View>
    </View>
  );

  export default Illustration;
