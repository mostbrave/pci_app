'use strict';

import firebase from 'firebase';
import React, { Component} from 'react';
import {
  Text,
  Button,
  View,
  StyleSheet,
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons';

import {styles as Styles, main} from '../styles';


//This represents the headers of the pages: Dashboard, Reports, Swiper, Settings
const MenuButtonGenerator = ({menuHandler, menuTitle}) => (
  <View style={[main.container,{backgroundColor: '#0daddc'}]}>
    <Icon.Button
      name="ios-menu"
      color='#fff'
      backgroundColor='#0daddc'
      onPress={menuHandler}/>
    <Text style={Styles.navigatorText}>
      {menuTitle}
    </Text>
    <Text style={main.header_right}></Text>
  </View>
)

//This returns the date in the format YYYYMMDD. e.g. 20160405
export function getTodaysDate() {
    var today = new Date();
    var year = today.getFullYear();
    var month = (today.getMonth() + 1) < 10 ? ('0' + (today.getMonth() + 1)) : (today.getMonth() + 1);
    var day = today.getDate() < 10 ? ('0' + today.getDate()) : today.getDate();
    var todaysDate = year + '' + month + '' + day;

    return todaysDate;
}

/**
 * This method will return the current time in milliseconds
 */
export function getCurrentTimeinMilliseconds() {
    //Today's date based on user's time zone
    var todaysDate = new Date();
    //return the milliseconds time of that day snapshot in a variable
    return todaysDate.getTime();
}

/**
 * This method will return today's date in milliseconds
 * It will also ensure that the day is starting at
 * midnight where setHours (0,0,0,0)
 */
export function getTodaysDateinMilliseconds() {
    //Today's date based on user's time zone
    var todaysDate = new Date();
    //Reset the day to the beg of the day, that way, if for
    //some reason the app closes, user can come back, and rate
    //the actions again using the same getTime in milliseconds
    todaysDate.setHours(0, 0, 0, 0);
    //return the milliseconds time of that day snapshot in a variable
    return todaysDate.getTime();
}

/**
* This function takes the day as integer the way it comes from the date
* object, and it returns the string representation of the day as a three
* letter word
**/
export function getWeekDay(day){
  var weekdays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  if(day >=0 && day<7){
    return weekdays[day];
  }
}

/**
* This method takes a date in the form of milliseconds and converts
* it to a date in the form of MMM DD YYYY (e.g. Feb 02, 2014)
**/
export function getDateinReadableFormat(dateinmilliseconds) {
    var someDate = new Date();
    someDate.setTime(dateinmilliseconds);
    var months = ['Jan', 'Feb', 'Mar',
        'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep',
        'Oct', 'Nov', 'Dec'
    ];

    if (someDate) {
        var day = someDate.getDate() < 10 ?
            ('0' + someDate.getDate()) : someDate.getDate();
        var year = someDate.getFullYear();
        var month = months[someDate.getMonth()];
        return month + ' ' + day + ', ' + year;
    }

    return '';

}

/**
* This function moves a firebase node from old reference
* to a new reference. Be careful when using this method
* because it deletes the old reference. It is mainly used
* to make a backup of a node
**/
export function moveNode(oldRef, newRef) {
  var oldRefNode = firebase.database().ref(oldRef);
  var newRefNode = firebase.database().ref(newRef);

     oldRefNode.once('value').then(function(snap) {
          newRefNode.update( snap.val(), function(error) {
               if( !error ) {  oldRefNode.remove(); }
               else if( typeof(console) !== 'undefined' && console.error ) {  console.error(error); }
          });
     });
}

export {
  MenuButtonGenerator
}
