'use strict';

  import React from 'react';
  import { View, Text, StyleSheet, Image } from 'react-native';
  import * as Progress from 'react-native-progress';
  import {theme, theme_vars} from '../styles';

  const styles = StyleSheet.create({
    regularText:{
      fontSize: 16,
      color: theme_vars[theme].activeTextColor
    },
    smallText:{
      fontSize: 10,
      color: theme_vars[theme].activeTextColor
    }
  });


  const SomethingWrong = (props) => (
    <View style={{flex:1, alignItems: 'center',
  justifyContent: 'center', overflow: 'hidden'}}>
    <View>
      <Image
      source={require( '../../assets/something_wrong.png')}
      resizeMode="contain"/>
      </View>
      <View style={{alignItems: 'center',
    justifyContent: 'center'}}>
      <Text style={styles.regularText}>
        Aaaah! Something went wrong
      </Text>
      <Text style={styles.smallText}>
        If the error persists, please try resetting your profile
      </Text>
      </View>
    </View>
  );

  export default SomethingWrong;
