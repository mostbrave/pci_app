'use strict';

import React, { Component} from 'react';
import {
  View,
  StyleSheet,
} from 'react-native'

import * as Progress from 'react-native-progress';

import {styles as Styles} from '../styles';

/**
* This is used by all pages where there is a need to
* show a wait with a looping circle
*
*/
export const ProgressCircle = (props) => (
<View style={{flex: 8,
  backgroundColor: 'white',alignItems: 'center',
justifyContent: 'center'}}>
<Progress.CircleSnail color={[ 'green','blue', 'red']} />
</View>
);

/**
* This function takes a @param num between 1-100
* @returns the colors:
* red: 1->33
* Orange: 34 -> 67
* Green: 67->100
*/
export var progressColor = (num) => {
  console.log(num);
  if(!num || num < 34){
    return 'red';
  }else if(num < 68){
    return 'orange';
  }else{
    return 'green';
  }
}

/**
* This function takes the @param num
* that represents a day score which is between 0 and 7
* where 0 is the highest day score at 100%
* and 7 is lowest days scope at 0%
*
* @returns the colors:
* red: 1->33
* Orange: 34 -> 67
* Green: 67->100
*/
export var dayProgressColor = (num) => {
  //ensure num is valid
  if(!num || num <0){
    num=7;
  }
  var numPercentage = 100 - (num.toFixed(0) * 100/7);
  return progressColor(numPercentage);
}
