'use strict'

import React, {Component, PropTypes,} from 'react';

import {View, StyleSheet, Text} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const styles = StyleSheet.create({

});

const emptystar = 'ios-star-outline';
const halfstar = 'ios-star-half-outline';
const fullstar = 'ios-star';



function getStar(iconName){
  return <Icon name={iconName} color="yellow" />;
}

class Rating extends Component{
  static propTypes = {
    color: PropTypes.string,
    showsText: PropTypes.bool,
    innerText: PropTypes.string,
    size: PropTypes.number,
    textStyle: PropTypes.any,
    score: PropTypes.number,
  };

  static defaultProps = {
    color: 'rgba(0, 122, 255, 1)',
    showsText: false,
    size: 20,
  };
}

export class ScoreRating extends Rating {
  constructor(props) {
    super(props);
  }

  render() {
    let {
      color,
      showsText,
      innerText,
      size,
      textStyle,
    } = this.props;

    if(this.props.size >=6){

    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <View style={[styles.container, {flexDirection: 'row'}]}>
        <View>{getStar(halfstar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
      </View>
      <View>
        <Text style={this.props.textStyle}>Very Poor</Text>
      </View>
      </View>
    );
  }else if(this.props.size>=4){
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <View style={[styles.container, {flexDirection: 'row'}]}>
        <View>{getStar(fullstar)}</View>
        <View>{getStar(fullstar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
        </View>
          <View>
            <Text style={this.props.textStyle}>Poor</Text>
          </View>
      </View>

    );
  }else if(this.props.size >= 2){
    return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
      <View style={[styles.container, {flexDirection: 'row'}]}>
        <View>{getStar(fullstar)}</View>
        <View>{getStar(fullstar)}</View>
        <View>{getStar(fullstar)}</View>
        <View>{getStar(emptystar)}</View>
        <View>{getStar(emptystar)}</View>
        </View>
          <View>
            <Text style={this.props.textStyle}>Good</Text>
          </View>
      </View>
    );
  }else if(this.props.size==1){

      return (
        <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <View style={[styles.container, {flexDirection: 'row'}]}>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(emptystar)}</View>
        </View>
        <View><Text style={this.props.textStyle}>Very Good</Text></View>
        </View>
      );

  }else if(this.props.size==0){

      return (
      <View style={{alignItems: 'center', justifyContent: 'center'}}>
        <View style={[styles.container, {flexDirection: 'row'}]}>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
          <View>{getStar(fullstar)}</View>
        </View>
          <View>
            <Text style={this.props.textStyle}>Excellent</Text>
          </View>
      </View>
      );
  }

  }
}
