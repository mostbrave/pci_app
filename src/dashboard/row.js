import React from 'react';
import { View, Text, StyleSheet, Image } from 'react-native';
import * as Progress from 'react-native-progress';
import * as ProgressReport from '../generic/progress';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textView: {
    marginLeft: 10,
    flex: 8,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  text: {
    fontSize: 14,
    color: '#abb2bf',
    flexWrap: 'wrap',
  },
  option:{
    fontSize: 12,
    color: 'grey',
  },
  optionView:{
    marginLeft: 10,
    flex: 1,
  },
  rank: {
    fontSize: 28,
  },
  pie: {
    height: 60,
    width: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    flex: 1,
  },
  progress: {
    margin: 10,
  },
  progressText:{
    fontSize: 12,
    color: '#fff'
  }
});




//Orange Red #ff4500
//Green '#7fff00'
//GOld #ffd700
const Row = (props) => (
  <View style={styles.container}>
  <View style={styles.pie}>
   <Progress.Circle
     style={styles.progress}
     color={ProgressReport.progressColor(
       ((Number(props.animateprogress) <
         Number(props.subcatratios[props.items.subcatID])?
         Number(props.animateprogress):
         Number(props.subcatratios[props.items.subcatID]))*100).toFixed(0))}
     showsText={true}
     textStyle={styles.progressText}
     progress={Number(props.animateprogress) < Number(props.subcatratios[props.items.subcatID])?
       Number(props.animateprogress):Number(props.subcatratios[props.items.subcatID])}
     indeterminate={props.indeterminate}
   />
  </View>
  <View style={styles.textView}>
    <Text style={styles.text}>
      {props.items.subcatName}
    </Text>
    </View>

  </View>
);

export default Row;
