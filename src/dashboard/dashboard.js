import React, { Component, PropTypes } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles as Styles, main, theme, theme_vars} from '../styles';
import Row from './row';
import * as dashboardHelper from './dashboardhelper';
import * as loaddata from '../loaddata';
import Illustration from '../generic/illustration';
import * as ProgressReport from '../generic/progress';
import {routes} from '../constants';
import {MenuButtonGenerator} from '../generic/helpers';

import {
  Text,
  Button,
	TextInput,
  View,
  StyleSheet,
  ListView,
  TouchableHighlight
} from 'react-native'


export default class Dashboard extends Component {
  static propTypes = {
    menuHandler: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)
    this.state = {
      ready: false,
      progress: 0,
      indeterminate: true,
      timeouts: new Array(),
    }
  }

  componentDidMount(){
    dashboardHelper.getSubcatsInBestRatedOrder(function(topratedlist, subcatRatios){
      //Populate the list datasource with the array
      const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      this.setState ({
           dataSource: ds.cloneWithRows(topratedlist),
           ready: true,
           topratedlist: topratedlist,
           subcatratios: subcatRatios,
        });
        this.animate();
    }.bind(this))
  }

  animate() {
    let progress = Number(0);
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2})

      this.setState({ indeterminate: false })
      this.state.timeouts.push(setInterval(() => {
        progress += Number((Math.random() / 5).toFixed(2))
        if (progress > 1) {
          progress = 1
          this.state.timeouts.forEach(function(intervalID){
            clearInterval(intervalID);
          })
        }
        //Reset it again to the ListView can refresh again
        this.setState({
         dataSource: ds.cloneWithRows(this.state.topratedlist),
          progress: progress })
      }, 10))

  }

  componentWillUnmount () {
    this.state.timeouts.forEach(function(intervalID){
      clearInterval(intervalID);
    });
  }

  render() {
    const { navigator, menuHandler } = this.props

    if(!this.state.ready){
      return (
        <ProgressReport.ProgressCircle />
      );
    }else{
      if(Object.keys(this.state.topratedlist).length > 0){
    return (
      <View style={{flex:1}}>
      <MenuButton menuHandler={menuHandler} />
      <View style={styles.pageDescription}>
      <Icon name="ios-information-circle-outline" size={24} color='#333333'/>
      <Text style={{paddingLeft: 10, paddingRight: 3, fontSize: 12}}>
        List of top rated activities based on all-time percentage consistency
      </Text>

      </View>

      <View style={[Styles.container, {flex: 7}]}>
        <ListView
          enableEmptySections={true}
          dataSource={this.state.dataSource}
          renderRow={(data,sectionId, rowId, highlightRow) =>
            <Row items={data} animateprogress={Number(this.state.progress)}
              subcatratios={this.state.subcatratios}
              indeterminate={this.state.indeterminate}/>}
              renderSeparator={(sectionId, rowId) =>
            <View key={rowId} style={styles.separator} />}
            />
      </View>

      <View style={[Styles.container, {flex: 2, alignItems: 'center', justifyContent: 'center',
        flexDirection: 'row'}]}>
        <View style={{backgroundColor: 'green', flex: 1, height: 40,
          alignItems: 'center', justifyContent: 'center'}}>
          <TouchableHighlight
            onPress={()=> navigator.push(routes.tracker)}>
              <Text style={{color: '#fff', fontSize: 14}}>Track My Progress</Text>
          </TouchableHighlight>
        </View>
        <View style={{backgroundColor: 'purple', flex: 1, height: 40,
            alignItems: 'center', justifyContent: 'center'}}>
            <TouchableHighlight
              onPress={()=> navigator.push({...routes.reports, menuHandler})}>
              <Text style={{color: '#fff', fontSize: 14}}>Go to My Reports</Text>
            </TouchableHighlight>
          </View>
      </View>
    </View>
    )
  }else{
    return (
      <View style={{flex: 1}}>
        <MenuButton menuHandler={menuHandler} />
        <View style={{flex: 5,
          backgroundColor: theme_vars[theme].backgroundColor,
          alignItems: 'center', justifyContent: 'flex-start'}}>
          <Illustration
            width={300}
            height={250}
            image={require('../../assets/empty_data.png')}
            largeText={'Welcome to Personal Craziness Index!'}
            smallText={'Are you ready to start tracking your progress today?!'} />

          <View style={{backgroundColor: '#00acde', borderRadius: 10,
          padding: 2, marginTop: 10}}>
          <Button
            onPress={()=>this.props.navigator.push(routes.tracker)}
            title="Start Tracking Now!"
            color="#ffffff"
            accessibilityLabel="Learn more about this purple button"/>
          </View>
        </View>
      </View>

    );
  }
  }
  }
}

const MenuButton = ({menuHandler}) => (
  <MenuButtonGenerator
    menuHandler={menuHandler}
    menuTitle='Dashboard'/>
)


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    marginLeft: 12,
    fontSize: 16,
    fontWeight: 'bold',
  },
  photo: {
    height: 40,
    width: 40,
    borderRadius: 20,
  },
  separator: {
    height: StyleSheet.hairlineWidth,
    marginBottom: 1,
    borderBottomWidth: 1,
    backgroundColor: 'grey',
  },
  pageDescription: {
    flex: 0.5,
    flexDirection: 'row',
    backgroundColor: '#f1f1f1',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    padding: 10,
  }
});
