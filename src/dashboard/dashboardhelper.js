'use strict';

import firebase from 'firebase';
import Subcat from '../category/subcat';
import * as catutil from '../category/categoryhelper';

/**
 * This method gets the number of yes swipes the user has done
 * and sorts them in an ascending order
 * @return an array of subcat objects from high to low in terms of
 * best scored in a JSON form
 **/
export var getSubcatsInBestRatedOrder = function(callback) {
catutil.getFullSubcatInfo(function(subcatArray){

    var toprated = new Array();
    var subcatRatio = new Array();
    var ref = firebase.database().ref('/tracking/' +
        firebase.auth().currentUser.uid + '/aggregates/');

    ref.orderByChild('yes').on('value', function(snapshot) {
        console.log('NUM OF CHILDREN AGGREGARES ' + snapshot.numChildren());
        var topRatedWithSubcatInfo = new Array();
        if (snapshot && snapshot.numChildren() > 0) {
            snapshot.forEach(function(subcatRatings) {
                var childKey = subcatRatings.key;
                var childData = subcatRatings.val();
                var yesCount;
                var noCount;
                toprated.push(childKey);
                //This contains the yes:2 no:1 values
                Object.keys(childData).forEach(function(subcat) {
                    if (subcat && subcat.indexOf('yes') != -1) {
                        yesCount = childData[subcat];
                    } else {
                        noCount = childData[subcat];
                    }
                });
                var ratio;
                if(!yesCount){
                  yesCount=0;
                }
                if(!noCount){
                  noCount=0;
                }

                if(yesCount ==0 && noCount ==0){
                  ratio=0;
                }else {
                  //Make sure it is a Number
                  //and round it to two numbers
                  ratio =
                  Number((yesCount / (yesCount + noCount))).toFixed(2);
                }

                subcatRatio[childKey] = ratio;
            });
            console.log("In Reverse");
            toprated.reverse();
            //Populate Top Rated with Actual Subcat Objects
            toprated.forEach(function(subcatID){
              topRatedWithSubcatInfo [subcatID] = subcatArray[subcatID];
            });
        }
        console.log('length '+ Object.keys(topRatedWithSubcatInfo).length);
        console.log(topRatedWithSubcatInfo);

        callback(topRatedWithSubcatInfo, subcatRatio);
    });
});
} //end of method
