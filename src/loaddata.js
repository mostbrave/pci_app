import firebase from 'firebase';

//This is used to load the initial data into the database
//Only use when you want to refresh static data
export var loadData = function(){
  var jsonFile = require('./schema.json');
  var ref = firebase.database().ref('/');
  ref.set(jsonFile);
}
