import { StyleSheet } from 'react-native'
const constants = {
  actionColor: '#24CE84',
	checkBoxColor: '#fff',
  backgroundColor: '#fff',
}

const theme_vars = {
  dark: {
    backgroundColor: '#FFFFFF',
    textColor: '#fff',
    colorDisabled: '#CCC',
    navigatorBackground: '#0daddc',
    statusBackground: '#111',
    fontWeight: 'bold'
  },

  pciDark: {
    backgroundColor: '#282c34',
    textColor: '#fff',
    colorDisabled: '#CCC',
    navigatorBackground: '#0daddc',
    statusBackground: '#111',
    fontWeight: 'bold',
    //this is for the scroll view tabs
    //Used for instance in Reports
    activeTextColor: '#fff',
    inactiveTextColor: 'red',
    bodyText: '#fff',
    reportLabelsColor: '#fff'
  }

}

const theme = 'pciDark';

const styles = StyleSheet.create({
  bodyText:{
    color: theme_vars[theme].bodyText
  },
  imageContainer: {
    flex: 1,
    justifyContent: 'center', alignItems: 'center', resizeMode: 'cover',
    width: null, height: null
  },
  container: {
    backgroundColor: theme_vars[theme].backgroundColor,
    flex: 1
  },
  ftuxContainer: {
    backgroundColor: 'white',
    flex: 1
  },
  containerNavMargin: {
    marginTop: 62
  },
  navigator: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme_vars[theme].navigatorBackground
  },
  navigatorText: {
    fontSize: 13,
    marginTop: 10,
    fontWeight: theme_vars[theme].fontWeight,
    color: theme_vars[theme].textColor
  },
  navigatorTextDisabled: {
    fontSize: 11,
    marginTop: 15,
    color: theme_vars[theme].colorDisabled
  },
  listview: {
    flex: 1,
  },
  li: {
    backgroundColor: '#FFF',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    paddingLeft: 15,
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 5,
    marginBottom: 5
  },
  liHeader: {
    backgroundColor: '#fff',
    borderColor: 'transparent',
    borderWidth: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 5,
  },
  liContainer: {
    flex: 2,
  },
  liHeaderText: {
    justifyContent: 'center',
    alignItems: 'center',
    color: '#333',
    fontSize: 21,
    textAlign: 'center',
    marginTop: 25,
    fontWeight: 'bold'
  },
	liText: {
    color: '#333',
    fontSize: 18,
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'left',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    lineHeight: 18,
  },
	checkbox: {
		width: 16,
		height: 16
	},
  navbar: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderBottomColor: '#eee',
    borderColor: 'transparent',
    borderWidth: 1,
    justifyContent: 'center',
    height: 44,
    flexDirection: 'row'
  },
  navbarTitle: {
    color: '#444',
    fontSize: 16,
    fontWeight: "500"
  },
  statusbar: {
    backgroundColor: '#fff',
    height: 22,
  },
  center: {
    textAlign: 'center',
  },
  actionText: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
  },
  action: {
    backgroundColor: constants.actionColor,
    borderColor: 'transparent',
    borderWidth: 1,
    paddingLeft: 16,
    paddingTop: 14,
    paddingBottom: 16,
  },
  input: {
    marginTop: 5,
    color: '#555',
		fontSize: 11
	},
  inputValue: {
    fontWeight: 'bold'
	},
  picker: {
    backgroundColor: '#fff'

  },
  pickerValue: {
    color: '#fff',
  },
  inputValueWicon: {
    marginRight: 15,
  },
  helpContainerStyle: {
    marginTop: 15
  },
  helpStyle: {
    color: '#555',
    fontSize: 12,
  }
})

const SummaryStyle = StyleSheet.create({
  userDataContainer: {
    borderRadius: 2,
    backgroundColor: '#333',
    padding: 20,
    marginTop: 15,
    marginLeft: 10,
    marginRight: 10,
    marginBottom: 5,
    shadowRadius: 1,
    shadowOpacity: .5,
    shadowColor: '#111',
  },
  userData: {
    color: '#FFF',
    fontSize: 12,
    marginBottom: 15,
  },
  userDataLbl: {
    fontWeight: 'bold'
  },
  listview: {
    flex: 1,
  },
  li: {
    backgroundColor: '#FFF',
    borderColor: '#ccc',
    borderBottomWidth: 1,
    paddingLeft: 15,
    paddingTop: 15,
    paddingBottom: 15,
    marginTop: 5,
    marginBottom: 5,
  },
  liHeader: {
    backgroundColor: '#111',
    borderColor: 'transparent',
    borderWidth: 1,
    paddingLeft: 15,
    paddingTop: 5,
    paddingBottom: 5,
    marginBottom: 5,
  },
  subcategories: {
    padding: 10,
  },
  liContainer: {
    flex: 2,
  },
  liHeaderText: {
    color: '#FFF',
    fontSize: 12,
  },
	liText: {
    color: '#333',
    fontSize: 18,
  },
})

const InfoStyle = StyleSheet.create({
  tocContainer: {
    backgroundColor: '#FFF',
  },
  tocScrollView: {
    height: 200,
    padding: 20,
  },
  tocText: {
    fontSize: 11,
    height: 1000,
  }
})

const CategoriesStyle = StyleSheet.create({
  countContainer: {
    backgroundColor: '#fff',
    padding: 10,
  },
  count: {
    textAlign: 'center',
    color: '#333',
    fontWeight: 'bold',
    fontSize: 14,
  },
})

const InfoBoxStyle = StyleSheet.create({
  container: {
    padding: 20,
    marginBottom: 15,
  },
  info: {
    backgroundColor: '#2168f7',
  },
  error: {
    backgroundColor: '#f71f07',
  },
  warning: {
    backgroundColor: '#f5bd07',
  },
  close: {
    textAlign: 'right',
  },
})

const main = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#FFFFFF',
    paddingTop: 20,
	},
  content: {
    marginTop: 20,
    marginLeft: 10,
	},
  header_left: {
    textAlign: 'left'
  },
  header_title: {
    fontFamily: 'Menlo',
    paddingTop: 10,
    fontSize: 14,
    color: '#fff',
  },
  header_right: {
    width: 45,
    paddingTop: 10,
  },
  splash: {
    width: 300,
    height: 600
  }
})

export {
  CategoriesStyle,
	constants,
  main,
  InfoBoxStyle,
  InfoStyle,
	styles,
  SummaryStyle,
  theme_vars,
  theme
}
