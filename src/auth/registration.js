import React, { Component, PropTypes } from 'react'
import * as firebase from 'firebase'
import RegistrationForm from './details'
import * as Exceptions from '../error-codes'
import {register} from './auth-service'
import {styles as Styles} from '../styles'

import {
	Image,
	Keyboard,
  StatusBar,
  StyleSheet,
  Text,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Registration extends Component {

  constructor(props) {
    super(props)

		this.state = {
      errorMsg: ''
    }
  }

	register(email, password) {
		register(email, password)
			.then((user) => {
				user.sendEmailVerification()
			})
			.catch((err) => {
	  		this.showErrorMsg(err.code)
			})
  }

	onRegister = (email, password) => {
    this.showErrorMsg()
		this.register(email, password)
	}

	showErrorMsg(error) {
    this.setState({errorMsg: error? Exceptions.code[error]: ''})
  }

  render() {
    return (
			<TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
				<View style={styles.container}>
					<StatusBar barStyle="default" />
					<Text style={styles.errorTxt}>
						{this.state.errorMsg}
					</Text>
					<RegistrationForm style={styles.form} onRegister={this.onRegister} />
				</View>
			</TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
		justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: '#333',
		flex: 1
	},
	signup: {
    color: '#000000'
  },
	signupLink: {
    color: '#0774FA'
  },
	signupAction: {
		justifyContent: 'center',
    alignItems: 'center',
    width: 350,
		height: 20
  },
	errorTxt: {
    marginTop: 100,
    color: '#ff0000'
  },
  form: {
		flex: 1,
		padding: 0
  }
});
