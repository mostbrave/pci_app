/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import * as firebase from 'firebase'
import LoginForm from './loginform'
import * as Exceptions from '../error-codes'
import {signin} from './auth-service'
import {styles as Styles} from '../styles'

import {
	Animated,
  AppRegistry,
	Image,
	Keyboard,
  StatusBar,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Auth extends Component {
  static propTypes = {
		onRegister: PropTypes.func.isRequired,
		onReset: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

		this.state = {
      errorMsg: '',
      bounceValue: new Animated.Value(0),
      bounceErrValue: new Animated.Value(0)
    }
  }

	componentDidMount() {
    this.state.bounceValue.setValue(1.5)
    Animated.spring(
      this.state.bounceValue,
      {
        toValue: 1,
        friction: 1,
      }
    ).start()
  }

	onLogin = (email, password) => {
    this.showErrorMsg()

		signin(email, password).catch((err) => {
      this.showErrorMsg(err.code)
    })
	}

	showErrorMsg(error) {
    this.setState({errorMsg: error? Exceptions.code[error]: ''})
  }

  render() {
    return (
			<TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
				<View style={styles.container}>
					<StatusBar backgroundColor="blue" barStyle="light-content"/>
					<View style={styles.logo_wrapper}>
						<Animated.Image style={{
							width: 127, height: 141,
							transform: [
								{scale: this.state.bounceValue}
							]
						}} source={require('../../assets/logo-main-scene.png')} />
					</View>

					<Text style={styles.errorTxt}>
						{this.state.errorMsg}
					</Text>

					<LoginForm style={styles.form} onLogin={this.onLogin} />

					<View style={styles.actions}>
						<TouchableHighlight
							underlayColor='transparent'
							style={styles.signupAction}
							onPress={this.props.onRegister}>
							<Text style={styles.signup}> Need an account?
								<Text style={styles.signupLink}> Signup now</Text>
							</Text>
						</TouchableHighlight>

						<TouchableHighlight
							underlayColor='transparent'
							style={styles.signupAction}
							onPress={this.props.onReset}>
							<Text style={styles.signup}>Forgot password?
								<Text style={styles.signupLink}> Reset password now</Text>
							</Text>
						</TouchableHighlight>
					</View>
				</View>
			</TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: '#333',
		flex: 1
	},
  logo_wrapper: {
    flex: 1,
    marginTop: 100
  },
	signup: {
    color: '#fff'
  },
	signupLink: {
    color: '#6fabf5'
  },
	actions: {
		flex: 3,
		marginTop: 25
  },
	signupAction: {
		marginTop: 15,
		justifyContent: 'center',
    alignItems: 'center',
    width: 350,
		height: 20
  },
	errorTxt: {
    marginTop: 100,
    color: '#ff0000'
  },
  form: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
		padding: 10
  }
});
