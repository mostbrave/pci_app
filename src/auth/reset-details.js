/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
  View
} from 'react-native'

export default class ResetDetails extends Component {
  static propTypes = {
    onReset: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      email: ''
    }
  }

  onNext = (ref) => {
    this.refs[ref].focus()
  }

  render() {
    return (
			<View style={this.props.style}>
				<View style={styles.inputWrapper} >
					<TextInput
            value={this.state.username}
            onChangeText={email => this.setState({email})}
						style={styles.input}
						placeholder={'Email'}
            placeholderTextColor= {'#fff'}
						keyboardType={'email-address'}
            autoCapitalize="none"
            autoCorrect={false}
            autoFocus={true}
            onSubmitEditing={() => this.props.onReset(this.state.email)}
					/>
				</View>
				<TouchableHighlight
          ref="submit"
					onPress={() => this.props.onReset(this.state.email)}
					style={styles.button}>
					<Text style={styles.buttonText}>Send Reset Email</Text>
				</TouchableHighlight>
			</View>
    );
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
		width: 300,
    marginBottom: 5,
	},
	input: {
    color: '#333',
		fontSize: 11,
		height: 45,
		padding: 5,
    paddingLeft: 10,
    textAlign: 'left',
    backgroundColor: '#fff'
	},
	button: {
		width: 115,
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#0774FA',
    borderRadius: 3,
		marginTop: 25
	},
	buttonText: {
		color: '#FFF',
    fontSize: 11
	}
});
