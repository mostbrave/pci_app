/**
 * @flow
 */

import * as firebase from 'firebase'

/**
 * Signout API call.
 * @returns promise
 */
function signout() {
	return firebase.auth().signOut()
}

/**
 * Signin API call.
 * @returns promise
 */
function signin(email: string, password: string) {
	return firebase.auth().signInWithEmailAndPassword(email, password)
}

/**
 * Registration API call.
 * @returns promise
 */
function register(email: string, password: string) {
	return firebase.auth().createUserWithEmailAndPassword(email, password)
}

/**
 * Update password API call.
 * @returns promise
 */
function updatePassword(newPassword: string) {
	const user = firebase.auth().currentUser
	return user.updatePassword(newPassword)
}

/**
 * Send a password reset email API call.
 * @returns promise
 */
function resetPassword(email: string) {
	return firebase.auth().sendPasswordResetEmail(email)
}

export {
	signin,
	signout,
	register,
	resetPassword,
	updatePassword
}
