/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import * as firebase from 'firebase'
import ResetDetails from './reset-details'
import * as Exceptions from '../error-codes'
import {resetPassword} from '../auth/auth-service'
import {styles as Styles} from '../styles'

import {
	Animated,
  AppRegistry,
	Image,
	Keyboard,
  StatusBar,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
	TouchableWithoutFeedback,
  View
} from 'react-native'

export default class Reset extends Component {
	static propTypes = {
		navigator: PropTypes.object
  }

  constructor(props) {
    super(props)

		this.state = {
      errorMsg: ''
    }
  }

	resetPassword(email) {
		resetPassword(email)
			.then(() => {
				this.props.navigator.pop()
			})
			.catch((err) => {
	  		this.showErrorMsg(err.code)
			})
  }

	onReset = (email) => {
    this.showErrorMsg()
		this.resetPassword(email)
	}

	showErrorMsg(error) {
    this.setState({errorMsg: error? Exceptions.code[error]: ''})
  }

  render() {
    return (
			<TouchableWithoutFeedback onPress={()=> Keyboard.dismiss()}>
				<View style={styles.container}>
					<StatusBar barStyle="default" />
					<Text style={styles.errorTxt}>
						{this.state.errorMsg}
					</Text>
					<ResetDetails style={styles.form} onReset={this.onReset} />
				</View>
			</TouchableWithoutFeedback>
    )
  }
}

const styles = StyleSheet.create({
  container: {
		justifyContent: 'center',
    alignItems: 'center',
		backgroundColor: '#333',
		flex: 1
	},
	signup: {
    color: '#fff'
  },
	signupLink: {
    color: '#0774FA'
  },
	signupAction: {
		justifyContent: 'center',
    alignItems: 'center',
    width: 350,
		height: 20
  },
	errorTxt: {
		marginTop: 100,
    color: '#ff0000'
  },
  form: {
    flex: 1,
		padding: 0
  }
});
