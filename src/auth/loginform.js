/**
 * @flow
 */

import React, { Component, PropTypes } from 'react'
import {
  AppRegistry,
  StyleSheet,
  Text,
	TextInput,
	TouchableHighlight,
  View
} from 'react-native'

export default class LoginForm extends Component {
  static propTypes = {
    onLogin: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props)

    this.state = {
      email: '',
      password: ''
    }
  }

  onEmailSubmit = () => {
    this.refs['pwd'].focus()
  }

  render() {
    return (
			<View style={this.props.style}>
				<View style={styles.inputWrapper} >
					<TextInput
            value={this.state.username}
            onChangeText={email => this.setState({email})}
						style={styles.input}
            autoCapitalize="none"
            autoCorrect={false}
						placeholder={'Email'}
            placeholderTextColor= {'#333333'}
						keyboardType={'email-address'}
            onSubmitEditing={this.onEmailSubmit}
					/>
				</View>
				<View style={styles.inputWrapper} >
					<TextInput
            ref="pwd"
						style={styles.input}
						placeholder={'Password'}
            placeholderTextColor= {'#333333'}
            autoCapitalize="none"
						secureTextEntry={true}
            autoCorrect={false}
            value={this.state.password}
            onChangeText={password => this.setState({password})}
            onSubmitEditing={() => this.props.onLogin(this.state.email, this.state.password)}
					/>
				</View>
				<TouchableHighlight
					onPress={() => this.props.onLogin(this.state.email, this.state.password)}
					style={styles.button}>
					<Text style={styles.buttonText}>LOGIN</Text>
				</TouchableHighlight>
			</View>
    );
  }
}

const styles = StyleSheet.create({
  inputWrapper: {
		width: 300,
    marginBottom: 5,
	},
	input: {
    color: '#333',
		fontSize: 11,
		height: 45,
		padding: 5,
    paddingLeft: 10,
    textAlign: 'left',
    backgroundColor: '#fff'
	},
	button: {
		width: 75,
		height: 30,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#0774FA',
    borderRadius: 3,
		marginTop: 25
	},
	buttonText: {
		color: '#FFF',
    fontSize: 11
	}
});
