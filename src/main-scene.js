import React, { Component, PropTypes } from 'react';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import Dashboard from './dashboard/dashboard'
import Reports from './reports/reports';
import Profile from './profile/profile';
import DailyTracker from './tracking/dailytrackerswiper';
import {Settings} from './settings/settings';
import Icon from 'react-native-vector-icons/Ionicons';
import {OffCanvasReveal} from 'react-native-off-canvas-menu';
import * as firebase from 'firebase';
import Help from './help/home';

import {
  StyleSheet,
  Text,
  View
} from 'react-native'

export default class MainScene extends Component {
  static propTypes = {
    user: PropTypes.object.isRequired,
    menuHandler: PropTypes.func.isRequired,
  }

  render() {
    var currentusr = firebase.auth().currentUser;

    //This holds the user display name which tells the user which
    //user is logged in
    var displayName='';
    if(currentusr && currentusr.displayName && currentusr.displayName!=''){
      displayName = currentusr.displayName;
    }

    const {navigator} = this.props
    console.log("\n\nMain Scene\n\n");
    console.log(this.props.menuHandler);
    return (
      <View style={{flex: 1}}>
        <OffCanvasReveal
          active={this.props.menuOpen}
          onMenuPress={this.props.menuHandler}
          backgroundColor={'#333333'}
          menuTextStyles={{color: '#CCCCCC'}}
          handleBackPress={true}
          menuItems={[
            {
              title: 'Dashboard',
              icon: <Icon name="ios-globe-outline" size={35} color='#ffffff' />,
              renderScene: <Dashboard navigator={navigator} menuHandler={this.props.menuHandler}/>
            },
            {
              title: 'Tracker',
              icon: <Icon name="ios-checkmark-circle-outline" size={35} color='#ffffff' />,
              renderScene: <DailyTracker navigator={navigator} menuHandler={this.props.menuHandler}/>
            },
            {
              title: 'Reports',
              icon: <Icon name="ios-stats-outline" size={35} color='#ffffff' />,
              renderScene: <Reports navigator={navigator} menuHandler={this.props.menuHandler}/>
            },
            {
              title: 'Account' + ' (' + (displayName) + ')',
              icon: <Icon name="ios-contact-outline" size={35} color='#ffffff' />,
              renderScene: <Settings navigator={navigator} menuHandler={this.props.menuHandler}/>
            },
            {
              title: 'Help',
              icon: <Icon name="ios-help-circle-outline" size={35} color='#ffffff' />,
              renderScene: <Help navigator={navigator} menuHandler={this.props.menuHandler}/>
            },
          ]}
        />
      </View>
    )
  }
}

const drawerStyles = {
  drawer: {
    backgroundColor: '#a9c'
  },
  main: {
    width: 300,
    marginTop: 60,
    backgroundColor: '#ccc'
  },
}

const styles = StyleSheet.create({
  text: {color: "#000000"},
  navigationContainer: {
    width: 300,
    marginTop: 60,
    backgroundColor: '#111'
	}
})
