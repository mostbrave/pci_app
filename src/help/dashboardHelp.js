import React, { Component, PropTypes } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles as Styles} from '../styles';
import {MenuButtonGenerator} from '../generic/helpers';
import Swiper from 'react-native-swiper';

import {
  Text,
  View,
  Image,
  TouchableOpacity
} from 'react-native';

export default class DashboardHelp extends Component {

  constructor() {
    super()
    this.state = {
    }
  }

  render() {
    const menuButton=<MenuButtonGenerator
    menuHandler={this.props.menuHandler}
    menuTitle='Dashboard Help'/>;
    return (
      <View style={[Styles.container,{borderWidth: 1, borderColor: 'yellow'}]}>
        {menuButton}
        <Swiper loop={false}>
        <Image source={require('../../assets/instruction_main_dashboard.png')}
         key={0} />

        </Swiper>
      </View>
    );
  }
}
