import React, { Component, PropTypes } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import {styles as Styles} from '../styles';
import {MenuButtonGenerator} from '../generic/helpers';
import {routes} from '../constants';
import DashboardHelp from './dashboardHelp';
import {
  Text,
  View,
  TouchableOpacity
} from 'react-native';

export default class Help extends Component {

  constructor() {
    super()
    this.state = {
    }
  }

  render() {
    const menuButton=<MenuButtonGenerator
    menuHandler={this.props.menuHandler}
    menuTitle='Help'/>;
    return (
      <View style={[Styles.container,{borderWidth: 1, borderColor: 'yellow'}]}>
        {menuButton}
        <View style={{flex: 1, flexDirection: 'row',
      justifyContent: 'space-between', alignItems: 'center'}}>
          <View style={{flex: 1, flexDirection: 'column', alignItems: 'center',
          justifyContent: 'center'}}>
            <TouchableOpacity onPress = {()=>this.props.navigator.push(routes.dashboardHelp)}>
              <Icon name="ios-globe-outline" size={70} color='#ffffff' />
              <Text style={Styles.bodyText}>Dashboard</Text>
            </TouchableOpacity>
          </View>

          <View style={{flex: 1, flexDirection: 'column', alignItems: 'center',
          justifyContent: 'center'}}>
            <Icon name="ios-checkmark-circle-outline" size={70} color='#ffffff' />
            <Text style={Styles.bodyText}>Tracker</Text>
          </View>
        </View>
        <View style={{flex: 1, flexDirection: 'row',
        justifyContent: 'space-between', alignItems: 'center'}}>
          <View style={{flex: 1, flexDirection: 'column', alignItems: 'center',
          justifyContent: 'center'}}>
            <Icon name="ios-stats" size={70} color='#ffffff' />
            <Text style={Styles.bodyText}>Reports</Text>
          </View>
          <View style={{flex: 1, flexDirection: 'column', alignItems: 'center',
          justifyContent: 'center'}}>
            <Icon name="ios-contact" size={70} color='#ffffff' />
            <Text style={Styles.bodyText}>Account</Text>
          </View>
        </View>
      </View>
    );
  }
}
