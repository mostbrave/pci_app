const en = {
	next: 'Next',
	back: 'Back',
	finish: 'Finish'
}

const routes = {
  init: {
    name: 'initialization'
  },
  register: {
    name: 'register',
    title: 'Registration',
    showLeftNav: true
  },
	dashboardHelp:{
		name: 'dashboardHelp',
    title: 'Dashboard Help',
    showLeftNav: true
	},
  reset: {
    name: 'reset',
    title: 'Reset Password',
    showLeftNav: true
  },
  login: {
    name: 'login',
		title: 'Personal Craziness Index',

  },
  onboarding: {
    name: 'onboarding'
  },
  ftux: {
    name: 'ftux'
  },
  main: {
    name: 'main'
  },
  dashboard: {
    name: 'dashboard'
  },
  tracker: {
    name: 'tracker'
  },
  reports: {
    name: 'reports'
  },
  account: {
    name: 'account'
  },
}

export {
	en,
	routes,
}
