import {styles as Styles,
  theme_vars, theme} from '../styles';

export const xKey = "name";
export const yKey="y";

export const barChartOptions = {
  width: 300,
  height: 110,
  margin: {
    top: 20,
    left: 25,
    bottom: 50,
    right: 20
  },
  color: '#0cb027',
  gutter: 20,
  animate: {
    type: 'oneByOne',
    duration: 200,
    fillTransition: 3
  },
  axisX: {
    showAxis: true,
    showLines: true,
    showLabels: true,
    showTicks: true,
    zeroAxis: false,
    orient: 'bottom',
    label: {
      fontFamily: 'Arial',
      fontSize: 9,
      fontWeight: true,
      fill: theme_vars[theme].reportLabelsColor
    }
  },
  axisY: {
    showAxis: true,
    showLines: true,
    showLabels: true,
    showTicks: true,
    zeroAxis: false,
    orient: 'left',
    label: {
      fontFamily: 'Arial',
      fontSize: 8,
      fontWeight: true,
      fill: theme_vars[theme].reportLabelsColor
    }
  }
}

export const lineOptions = {
  width: 280,
  height: 170,
  color: '#2980B9',
  margin: {
    top: 10,
    left: 35,
    bottom: 30,
    right: 10
  },
  animate: {
    type: 'delayed',
    duration: 1500
  },
  axisX: {
    showAxis: true,
    showLines: false,
    showTicks: true,
    showLabels: false,
    zeroAxis: true,
    orient: 'bottom',
    tickValues: [],
    label: {
      fontFamily: 'Arial',
      fontSize: 8,
      fontWeight: true,
      fill: theme_vars[theme].reportLabelsColor
    }
  },
  axisY: {
    showAxis: true,
    showLines: true,
    showLabels: true,
    showTicks: true,
    zeroAxis: true,
    orient: 'left',
    tickValues: [],
    label: {
      fontFamily: 'Arial',
      fontSize: 8,
      fontWeight: true,
      fill: theme_vars[theme].reportLabelsColor
    }
  }
}
