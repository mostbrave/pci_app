import React, { Component, PropTypes  } from 'react';
import firebase from 'firebase';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
    ScrollView,
} from 'react-native';

import * as Scores from '../calculator/score';
import * as Progress from 'react-native-progress';
import * as ProgressReport from '../generic/progress';
import Circle from './Circle';
import { Bar,StockLine } from 'react-native-pathjs-charts'
import Icon from 'react-native-vector-icons/Ionicons';
import * as options from './options';
import {
  ScoreRating,
} from '../generic/rating';

import Chart from 'react-native-chart';

import ScrollableTabView, { ScrollableTabBar, } from 'react-native-scrollable-tab-view';

import * as reportUtil from './reportsutil';

import {styles as Styles,
  main, theme_vars, theme} from '../styles';
import {MenuButtonGenerator} from '../generic/helpers';

const styles = StyleSheet.create({
  tabView: {
      flex: 1,
      padding: 10,
      backgroundColor: 'rgba(0,0,0,0.01)',
    },
    chart: {
        width: 350,
        height: 160,
    },

  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingVertical: 20,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color: theme_vars[theme].activeTextColor
  },
  circles: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  progress: {
    margin: 30,
  },
});

export default class Reports extends Component {

  static propTypes = {
    menuHandler: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);

    this.state = {
      progress: 0,
      indeterminate: true,
      isLoading: 0,
      dayScore: 0,
      dayScoreDate: 'Day',
      showDayScore: true,
      weekData: [[{[options.yKey]:1,[options.xKey]:"Day"}]],
      showWeekReport: true,
      weeksData: [[{[options.yKey]:1,[options.xKey]:1}]],
      showWeeksReport: true,
      yearData: [[{[options.yKey]:1,[options.xKey]:1}]],
      showYearReport: true,
      timeouts: new Array(),
    };
  }

  componentDidMount() {
    this.animate();
  }

  animate() {

      //Generate the reports

      //This will get the day report
      //(first section of the report's page)
      reportUtil.getDayReportData(function(daySnapshot) {
        console.log('Setting Day State ' + firebase);
          //If there is not data to Display
          //first time using app
          if (!daySnapshot) {
              this.setState({
                  showDayScore: false,
                  //Function returned, this will prevent
                  //screen to get into an infinite loading state
                  isLoading: this.state.isLoading + 1,
              });
          } else {

            this.setState({
                dayScore: daySnapshot.dayScore,
                dayScoreDate: daySnapshot.date,
                isLoading: this.state.isLoading + 1,
                indeterminate: false
            });

              let progress = Number(0);

              this.state.timeouts.push(setInterval(() => {
                progress += Number((Math.random() / 5).toFixed(2));
                if (progress > daySnapshot.dayScore) {
                  progress = daySnapshot.dayScore
                  this.state.timeouts.forEach(function(intervalID){
                    clearInterval(intervalID);
                  })
                }
                //Set the progress value to reflect progress incrementation
                this.setState({
                  progress: progress })
              }, 10));

          }
      }.bind(this));

      //Another call to the database to get the week's report
      //This will generate the week report
      reportUtil.getWeekReportData(function(weekReport) {
          if (!weekReport) {
              this.setState({
                  showWeekReport: false,
                  //Function returned, this will prevent
                  //screen to get into an infinite loading state
                  isLoading: this.state.isLoading + 1,
              });
          } else {
              this.setState({
                  weekReportDate: weekReport.weekof,
                  weekData: [weekReport.data],
                  isLoading: this.state.isLoading + 1,
              });
          }
      }.bind(this));

      //get the 6-week report
      reportUtil.getMultipleWeeksReportData(6,false, function(weekReport) {
          if (!weekReport) {
              this.setState({
                  showWeeksReport: false,
                  //Function returned, this will prevent
                  //screen to get into an infinite loading state
                  isLoading: this.state.isLoading + 1,
              });
          } else {
              this.setState({
                  weeksData: weekReport.data,
                  isLoading: this.state.isLoading + 1,
              });
          }
      }.bind(this));

      //Get the last 52 weeks report
      reportUtil.getMultipleWeeksReportData(52, true, function(weekReport) {
          if (!weekReport) {
              this.setState({
                  showYearReport: false,
                  //Function returned, this will prevent
                  //screen to get into an infinite loading state
                  isLoading: this.state.isLoading + 1,
              });
          } else {
              this.setState({
                  yearData: weekReport.data,
                  isLoading: this.state.isLoading + 1,
              });
          }
      }.bind(this));


  }

  componentWillUnmount () {
    this.state.timeouts.forEach(function(intervalID){
      clearInterval(intervalID);
    });
  }


  render() {


//Example Data for Bar Chart
    // let data = [
    //   [{
    //     "y": 49,
    //     "name": "apple"
    //   }, {
    //     "y": 42,
    //     "name": "apple"
    //   }],
    //   [{
    //     "y": 69,
    //     "name": "banana"
    //   }, {
    //     "y": 62,
    //     "name": "banana"
    //   }],
    //   [{
    //     "y": 29,
    //     "name": "grape"
    //   }, {
    //     "y": 15,
    //     "name": "grape"
    //   }]
    // ]
//Example Data for line chart
    // let lineChart = [
    //   [{
    //     "x": 0,
    //     "y": 47782
    //   }, {
    //     "x": 1,
    //     "y": 48497
    //   }, {
    //     "x": 2,
    //     "y": 77128
    //   }, {
    //     "x": 3,
    //     "y": 73413
    //   }, {
    //     "x": 4,
    //     "y": 58257
    //   }, {
    //     "x": 5,
    //     "y": 40579
    //   }, {
    //     "x": 6,
    //     "y": 72893
    //   }],
    //   [{
    //     "x": 0,
    //     "y": 132189
    //   }, {
    //     "x": 1,
    //     "y": 61705
    //   }, {
    //     "x": 2,
    //     "y": 154976
    //   }, {
    //     "x": 3,
    //     "y": 81304
    //   }, {
    //     "x": 4,
    //     "y": 172572
    //   }, {
    //     "x": 5,
    //     "y": 140656
    //   }, {
    //     "x": 6,
    //     "y": 148606
    //   }],
    //   [{
    //     "x": 0,
    //     "y": 125797
    //   }, {
    //     "x": 1,
    //     "y": 256656
    //   }, {
    //     "x": 2,
    //     "y": 222260
    //   }, {
    //     "x": 3,
    //     "y": 265642
    //   }, {
    //     "x": 4,
    //     "y": 263902
    //   }, {
    //     "x": 5,
    //     "y": 113453
    //   }, {
    //     "x": 6,
    //     "y": 289461
    //   }]
    // ]

    const { menuHandler } = this.props.navigator.navigationContext.currentRoute

    const noData=<View>
        <Text style={styles.welcome}>Report Not Available</Text>
    </View>;

    //This will be true if the user is using the app
    //for the first time
    if(!this.state.showDayScore &&
      !this.state.showWeekReport &&
      !this.state.showWeeksReport &&
      !this.state.showYearReport){
        return (
          <View style={{flex: 1}}>
            <MenuButtonGenerator
              menuHandler={(e) => {
                console.log({stuff_again: 'test'});
                menuHandler() || this.props.menuHandler()
              }}
              menuTitle='Reports'/>
            <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={{flex:1, justifyContent: 'center'}}>
                <Text style={styles.welcome}>
                  No Data to Report
                </Text>
              </View>
            </View>
          </View>
        );
      }
    else if(this.state.isLoading < 4){
            return (
              <View style={styles.container}>
                <ProgressReport.ProgressCircle />
              </View>
            );
    }else{
    const rating = <ScoreRating size={this.state.dayScore}
    showsText={true} textStyle={{color: 'white'}}/>;
    console.log(this.state.weekData);
    return (
      <View style={{flex: 1}}>
        <MenuButtonGenerator
          menuHandler={(e) => {
            menuHandler(true) || this.props.menuHandler(true)
          }}
          menuTitle='Reports'/>
          <View style={[Styles.container, {flexDirection: 'column', flex: 19}]}>
              <View style={{flex: 1}}>
                  <ScrollableTabView style={{marginTop: 8, flex:1}}
                  renderTabBar={()=>
                      <ScrollableTabBar
                      activeTextColor={theme_vars[theme].activeTextColor}
                      inactiveTextColor={theme_vars[theme].activeTextColor}
                      />} >

                      <ScrollView
                        tabLabel={this.state.dayScoreDate}
                        style={styles.tabView}
                        >

                          <View style={styles.circles}>
                              {this.state.showDayScore?
                              <Circle style={[styles.progress]}
                              progress={1-(this.state.progress/Scores._totalPossibleDayPoints)}
                              thickness={2}
                              innerText={(this.state.progress).toFixed(0) + ' POINTS'}
                              showsInnerHeader={ true} innerHeader={rating}
                              size={140} showsText={true}
                              animated={true}
                              color={ProgressReport.dayProgressColor(this.state.progress)}
                                  indeterminate={this.state.indeterminate} /> :
                              noData
                              }
                          </View>
                      </ScrollView>

                  </ScrollableTabView>
              </View>
              <View style={{flex: 1, flexDirection: 'column'}}>
                  <ScrollableTabView style={{marginTop: 20, flex: 1}}
                  initialPage={0} renderTabBar={()=>
                      <ScrollableTabBar
                      activeTextColor={theme_vars[theme].activeTextColor}
                      inactiveTextColor={theme_vars[theme].activeTextColor}/>}>

                      <ScrollView tabLabel="WEEK" style={styles.tabView}>

                          <View style={styles.card}>
                              {this.state.showWeekReport?
                              <View>
                                  <View style={{alignItems: 'center',
                                  justifyContent: 'center'}}>
                                      <Text style={Styles.bodyText}>
                                        {this.state.weekReportDate}
                                      </Text>
                                  </View>
                                    <Bar data={this.state.weekData}
                                    options={options.barChartOptions}
                                    accessorKey={options.yKey}/>
                              </View>
                              :
                              noData
                              }
                          </View>

                      </ScrollView>


                      <ScrollView tabLabel="LAST 6 WEEKS" style={styles.tabView}>
                          <View style={styles.card}>
                              {this.state.showWeeksReport?
                              <Bar data={this.state.weeksData}
                              options={options.barChartOptions} accessorKey={options.yKey}/>
                               :
                            noData
                              }
                          </View>
                      </ScrollView>

                      <ScrollView tabLabel="LAST 52 WEEKS" style={styles.tabView}>
                          <View style={styles.card}>
                              {this.state.showYearReport?
                                <StockLine data={this.state.yearData}
                                options={options.lineOptions} xKey={options.xKey} yKey={options.yKey} />
                                 :
                              noData
                              }
                          </View>
                      </ScrollView>
                  </ScrollableTabView>
              </View>
          </View>
      </View>

    );
  }
}
}
