'use strict';

import * as Scores from '../calculator/score';
import firebase from 'firebase';
import * as helpers from '../generic/helpers';
import * as options from './options';

/**
 * This method gets the last progress rating
 * the system had recorded
 * @return an array object that represents a day score
 * and its corresponding date in milliseconds in the form of
 * {dateinmilliseconds:dayscore}
 **/
export var getDayReportData = function(callback) {
        try {
            var ref = firebase.database().ref('/tracking/' +
                firebase.auth().currentUser.uid + '/dailyscores/');

            var lastDate = ref.limitToLast(1);
            //This retrieves the timestamp of the last recorded
            //user rating activity
            lastDate.once('value', function(snapshot) {
                //Check if snapshot is not null
                console.log('Snapshot Exists? ' + snapshot.exists());
                //console.log('No of Records ' + snapshot.numChildren());
                console.log('SnapSHOOOOT length ' + JSON.stringify(snapshot.val()));
                if (snapshot && snapshot.exists()) {
                    //loop through the children, to retrieve the total
                    //number of points for all subcats, then substract them
                    //from the total possible day points
                    var dayScoreResult = {};
                    var dayScoreDate;
                    //start off with the total day points, and deduct any
                    //points the user has scored during that day
                    var dayScore = Scores._totalPossibleDayPoints;
                    snapshot.forEach(function(dateinmilliseconds) {
                        var childData = dateinmilliseconds.val();
                        dayScoreDate = helpers.getDateinReadableFormat(
                          dateinmilliseconds.key);
                        //console.log('Child Data: ' + JSON.stringify(childData));
                        //loop through the contents to retrieve the actual values
                        Object.keys(childData).forEach(function(subcat) {
                            //ensure the other properties like 'mode' do not
                            //get counted here
                            if (subcat && subcat.indexOf('subcat') != -1) {
                                //validate the scores before aggregating them
                                if (childData && childData[subcat] &&
                                    childData[subcat] < 2 &&
                                    childData[subcat] >= 0) {
                                    dayScore -= childData[subcat];
                                }
                            }
                        });
                    });

                    //validate to make sure the score returned is valid
                    if (typeof dayScore == 'number' &&
                        dayScore <= Scores._totalPossibleDayPoints &&
                        dayScore >= 0) {

                        var daySnapshot = {
                            'date': dayScoreDate,
                            'dayScore': dayScore
                        };
                        console.log('Should Have returned');
                        callback(daySnapshot);
                    }
                    //if Snapshot is empty
                } else {
                    console.log('Executed......................');
                    callback(false);
                }

            });
        } catch (exception) {
            console.error(exception.message);
            callback(false);
        }
    } //end of method


/**
 * This method retrieves the scores of the last rated week
 * which the user had completed rating
 * if the user is running a report during a particular week where
 * there is at least one day that is still not rated, the system will
 * retrieve the week before. Once the scoring completes for the whole week
 * then the system will pull it out on the report
 * @return a map object representation of scores for the whole week
 * e.g. {"weekof":"Dec 19, 2016","data":[["Mon",7],["Tue",7],["Wed",0],
 * ["Thu",6],["Fri",5],["Sat",3],["Sun",7],["Mon",3]]}

 **/
export var getWeekReportData = function(callback) {
    try {
        var ref = firebase.database().ref('/tracking/' +
            firebase.auth().currentUser.uid + '/dailyscores/');

        //Get the last 13 records. This will avoid making multiple calls
        //to the database. The logic is to get last 13 records, check if the
        //last record represents Sun, if it does not, then get the full
        //last week. 13 records guarrantee to get one full week, either
        //for the current week, and if the current week is not yet fully
        //rated, then get last week
        var lastTwoWeeks = ref.limitToLast(13);
        //get the last record, and retrieve its day of the week number
        //if it is a Sun, which corresponds to 6, then get the last 7
        //records that represents the current week, if not, get the first 7
        //which represents last week
        //One other case, if the user barely started using the app, and the
        //number of rows are less than 13, then get the first 7
        lastTwoWeeks.once('value', function(datesinmilliseconds) {
            //Check if snapshot is not null
            if (datesinmilliseconds && datesinmilliseconds.exists()) {
                //loop through the children, to retrieve the total
                //number of points for all subcats, then substract them
                //from the total possible day points
                var skip = true;
                var count = 0;
                var weekReport = {};
                var weekSubReport = [];
                var weekSubReportAggregate = [];

                console.log('Snapshot Exists? ' + datesinmilliseconds.exists());
                console.log('No of Records ' + datesinmilliseconds.numChildren());

                datesinmilliseconds.forEach(function(childSnapshot) {
                    // key will be value of the dates in milliseconds
                    var dateinmilliseconds = childSnapshot.key;
                    console.log('Value in Millisss ' + dateinmilliseconds);
                    //Check if the day is Monday if not keep skipping
                    var date = new Date();
                    date.setTime(dateinmilliseconds);
                    //This returns the day of the week
                    //0 for Sunday, 1 for Monday, 2 for Tuesday
                    //This will allow the loop to continue and calculate
                    //the daily values of the week
                    if (date.getDay() == 1 && skip) {
                        weekReport = {
                            'weekof': 'Week of: ' +
                                helpers.getDateinReadableFormat(dateinmilliseconds)
                        };
                        skip = false;
                    }

                    if (!skip && count < 7) {
                        //this holds the total points of each day
                        var dayTotal = Scores._totalPossibleDayPoints;

                        // childData will be the actual contents subcats and
                        //their corresponding values{subcat1:1, subcat2:0, etc}
                        var childData = childSnapshot.val();
                        //loop through the contents to retrieve the actual values
                        Object.keys(childData).forEach(function(subcat) {
                            //ensure the other properties like 'mode' do not
                            //get counted here
                            if (subcat && subcat.indexOf('subcat') != -1) {
                                //validate the scores before aggregating them
                                if (childData && childData[subcat] &&
                                    childData[subcat] < 2 &&
                                    childData[subcat] >= 0) {
                                    dayTotal -= childData[subcat];
                                }
                            }
                        });
                        //This is just a child node and represents a day score
                        var was={[options.xKey]:helpers.getWeekDay(date.getDay()), [options.yKey]:dayTotal};
                        weekSubReport.push(was);
                        count++;
                    } else if (count > 7) {
                        return true;
                    }

                });

                weekSubReportAggregate.push(weekSubReport);
                weekReport['data'] = weekSubReport;
                if (weekSubReport.length > 0) {
                  console.log('Week Report \n\n\n');
                  console.log(weekSubReportAggregate);
                  console.log('\n\n\n\n\n');
                    callback(weekReport);
                } else {
                    callback(false);
                }
            } else {
                callback(false);
                console.log('Executed......................');
            }
        });

    } catch (exception) {
        console.error(exception.message);
        console.log('we errored');
        callback(false);
    }
}

export var getSubcatHistory = function(subcatID, numofResults, callback) {
    try {
        var ref = firebase.database().ref('/tracking/' +
            firebase.auth().currentUser.uid + '/dailyscores/');

        //Get the last x # of records of a particular Subcat
        var subcatRecords = ref.limitToLast(numofResults);
        //This will retrieve all user tracking results for the last
        //x number of days as specified in the parameter
        subcatRecords.once('value', function(datesinmilliseconds) {
            //loop through the children, to retrieve the values
            //of that specific subcat ID on different days

            var subcatReport = {};
            var subcatSubReport = [];

            console.log('No of Records ' + datesinmilliseconds.numChildren());

            datesinmilliseconds.forEach(function(childSnapshot) {
                // key will be value of the dates in milliseconds
                var dateinmilliseconds = childSnapshot.key;
                //Check if the day is Monday if not keep skipping
                var date = new Date();
                date.setTime(dateinmilliseconds);

                // childData will be the actual contents subcats and
                //their corresponding values{subcat1:1, subcat2:0, etc}
                var childData = childSnapshot.val();
                //loop through the contents to retrieve the actual values
                Object.keys(childData).forEach(function(subcat) {
                    //ensure the other properties like 'mode' do not
                    //get counted here, and subcat matches
                    if (subcat && subcat.indexOf('subcat') != -1 &&
                        subcat.localeCompare(subcatID) == 0) {
                        //validate the scores before aggregating them
                        if (childData && childData[subcat] &&
                            childData[subcat] < 2 &&
                            childData[subcat] >= 0) {
                            weekSubReport.push(
                                [helpers.getWeekDay(date.getDay()),
                                    childData[subcat]
                                ]);
                        }
                    }
                });
            });
            weekReport['data'] = weekSubReport;
            callback(weekReport);
        });

    } catch (exception) {
        console.error(exception.message);
        console.log('we errored');
    }
}

/**
 * This method generates the monthly report broken down by weeks
 * @param: numofWeeks is an integer parameter represents the number
 * of weeks to retrieve to populate the data for the chart
 * @param: xnumbers is a boolean variable, it indicates whether
 * or not the x axis values should be numbers or can be strings
 * for instance a line chart requires numbers on the x axisColor
 * on the other hand, a bar chart you can have strings as labels
 * on the x axis
 **/
export function getMultipleWeeksReportData(numofWeeks, xnumbers,callback) {
    try {

        if (numofWeeks > 0) {
            var ref = firebase.database().ref('/tracking/' +
                firebase.auth().currentUser.uid + '/dailyscores/');

            //Get the last 13 records. This will avoid making multiple calls
            //to the database. The logic is to get last 13 records, check if the
            //last record represents Sun, if it does not, then get the full
            //last week. 13 records guarrantee to get one full week, either
            //for the current week, and if the current week is not yet fully
            //rated, then get last week
            var lastFewWeeks = ref.limitToLast((numofWeeks * 7) + 6);
            //get the last record, and retrieve its day of the week number
            //if it is a Sun, which corresponds to 6, then get the last 7
            //records that represents the current week, if not, get the first x
            //records which represents previous week(s)
            lastFewWeeks.once('value', function(datesinmilliseconds) {
                //Check if snapshot is not null
                if (datesinmilliseconds && datesinmilliseconds.exists() &&
                    datesinmilliseconds.numChildren()) {
                    //loop through the children, to retrieve the total
                    //number of points for all subcats, then substract them
                    //from the total possible day points
                    var searchingFirstWeekDay = true;
                    //Once the system is aggregating the database
                    //make this true, so the system continue looping
                    //to gather all the data
                    var shouldContinue = false;

                    var count = 1;
                    //xSerial is used as an increment number for the x axis
                    //values for a line chart
                    var xSerial=1;
                    var report = {};
                    var weekReport = [];
                    //This will hold the double array [[{x:2, y:4}, {x:3, y:8}, etc...]]
                    var weekReportAggregate=[];
                    //This will hold the first day of the week
                    //for reporting purposes
                    var firstDayofWeek = '';

                    //This will hold the aggregates for each week to represent
                    //the whole week score
                    var weekScore = 0;

                    console.log('No of Records: ' +
                        datesinmilliseconds.numChildren());

                    //aggregate the daily scores into a per-week score
                    datesinmilliseconds.forEach(function(childSnapshot) {

                        // key will be value of the dates in milliseconds
                        var dateinmilliseconds = childSnapshot.key;
                        //console.log('Test: ' + dateinmilliseconds);
                        //Check if the day is Monday if not keep skipping
                        var date = new Date();
                        date.setTime(dateinmilliseconds);

                        //This returns the day of the week
                        //0 for Sunday, 1 for Monday, 2 for Tuesday
                        //This will allow the loop to continue and calculate
                        //the daily values of the week
                        if (date.getDay() == 1 && (searchingFirstWeekDay ||
                                shouldContinue)) {
                            searchingFirstWeekDay = false;
                            //Allow Aggregation to continue
                            shouldContinue = true;
                            //Reset the week score
                            weekScore = 0;
                            //Store the first day of the week
                            firstDayofWeek = (date.getMonth() + 1) + '/' +
                                date.getDate();
                        }

                        if (!searchingFirstWeekDay && shouldContinue &&
                            count <= (numofWeeks * 7)) {
                            //this holds the total points of each day
                            var dayTotal = Scores._totalPossibleDayPoints;

                            //childData will be the actual contents subcats and
                            //their corresponding values{subcat1:1, subcat2:0, etc}
                            var childData = childSnapshot.val();
                            //loop through the contents to retrieve the actual values
                            Object.keys(childData).forEach(function(subcat) {
                                //ensure the other properties like 'mode' do not
                                //get counted here
                                if (subcat && subcat.indexOf('subcat') != -1) {
                                    //validate the scores before aggregating them
                                    if (childData &&
                                        typeof childData[subcat] == 'number' &&
                                        childData[subcat] < 2 &&
                                        childData[subcat] >= 0) {
                                        dayTotal -= childData[subcat];
                                    }
                                }
                            });
                            // console.log(dateinmilliseconds + ' ' + dayTotal);
                            // console.log('Week Score So Far: ' + weekScore);
                            //Aggregate the scores
                            weekScore += dayTotal;
                            //Add the record for the week score
                            if (count <= (numofWeeks * 7) &&
                                count % 7 == 0) {
                                //if the report if for more than one week, then
                                //represent the x axis labels as dates of month/day
                                if(xnumbers){
                                  weekReport.push({
                                    [options.xKey]:xSerial,
                                    [options.yKey]:weekScore});
                                }else {
                                  weekReport.push({
                                    [options.xKey]:firstDayofWeek,
                                    [options.yKey]:weekScore});
                                }
                                  //increment the x axis numbering
                                  xSerial++;
                            }
                            //Increment count of days covered
                            count++;
                        } else if (count > (numofWeeks * 7)) {
                            return true;
                        }
                    });
                    weekReportAggregate.push(weekReport);
                    report['data'] = weekReportAggregate;
                    console.log((xnumbers?'Year Report ':'Weeks ') + JSON.stringify(report));
                    if (weekReport.length > 0) {
                        callback(report);
                    } else {
                        callback(false);
                    }
                } else {
                    callback(false);
                }
            });
        }
    } catch (exception) {
        console.error(exception.message);
        console.log('we errored');
        callback(false);
    }
}

// /**
// * This method returns the number of thumbs up and
// * thumbs down for a particular subcategory
// * @param - subcat (e.g. subcat1, subcat2, etc...)
// * @returns - callback function with parameter
// * that holds an array of that form ['yes': 20, 'No':12]
// **/
// export var getSubcatAggregate = function(subcat, callback) {
//
//           var subcatAggregates = new Array();
//           //this will point to /aggregates/subcat1 for example to get the
//           //children info e.g. yes:20, no:2
//             var ref = firebase.database().ref('/tracking/' +
//                 firebase.auth().currentUser.uid + '/aggregates/' + subcat + '/');
//
//             ref.on('value', function(snapshot){
//                 var yes = snapshot.val().yes;
//                 var no = snapshot.val().no;
//                 subcatAggregates['yes']=yes;
//                 subcatAggregates['no']=no;
//
//               });
//               console.log('Aggregates for Subcat ' + subcat + ' are: ');
//               console.log(subcatAggregates);
//
//             callback(subcatAggregates);
//
//     } //end of method
