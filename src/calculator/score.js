'use strict';

//If user rates yes (thumbs up), the initial day score should
//decrease by one point
const handleYup = 1;
//If user rates no (thumbs down), the initial day score should
//not change
const handleNope = 0;

//Total day score, this represents the total number of
//points for one day that a user can get if all his reponses
//were all negative
//DO NOT MODIFY VALUE SINCE A LOT OF CALCULATIONS DEPEND ON IT
export const _totalPossibleDayPoints = 7;

//This contains the list of the ratings of daily scores
export var ratings = ['Poor', 'Moderate', 'Good', 'Excellent'];

export default class Score {

  constructor(score, subcatid){
    this.score=score;
    this.subcatid=subcatid;
  }

  //this allows us to access the constant variable from
  //other classes
  static getHandleYupValue(){
    return handleYup;
  }

  //this allows us to access the constant variable from
  //other classes
  static getHandleNopeValue(){
    return handleNope;
  }

}
