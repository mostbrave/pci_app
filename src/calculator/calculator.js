"use strict";

export default class Calculator{

  constructor (date, day, weekTotal, dayTotal, subcatScore){
    //the date of a particular day to be calculated
    this.date=date;
    //the day in string form. e.g Monday, Tuesday, etc...
    this.day=day;
    //This reflects the total of the week at a particular day.
    //The week total would not be complete until the full 7 days are reached
    //and calculated
    this.weekTotal=weekTotal;
    //This reflects how well the user performed at that particular day
    this.dayTotal=dayTotal;
    //This contains the scores of all the subcats that the user rates
    this.subcatScore=subcatScore;
  }

  

}
