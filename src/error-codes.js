export const code = {
  //Auth (Google Firebase) specific errors
  'auth/email-already-in-use': 'Email already in use!',
  'auth/invalid-email': 'Invalid email!',
  'auth/operation-not-allowed': 'Operation is not allowed!',
  'auth/weak-password': 'Weak Password :(',
  'auth/account-exists-with-different-credential': 'Account already exists!',
  'auth/timeout': 'Timeout error!',
  'auth/user-not-found': 'oooh no! User not found :(',
  'auth/wrong-password': 'oooh no! Wrong password :(',

  //App specific errors
  'no-first': 'First name is required',
  'no-last': 'Last name is required',
  'no-dob': 'DOB is required',
}
