'use strict';

export default class Category {

  constructor (categoryID, categoryName, categoryDesc, categoryImg) {
    //This represents the id of the subcategory. e.g subcat33
    this.categoryID = categoryID;

    //this represents the name of the subcategory. e.g. Have you excercised?
    this.categoryName = categoryName;

    //this represents the ID of the parent category
    this.categoryDesc = categoryDesc;

    //this represents the all time total number of thumbs up
    this.categoryImg=categoryImg;

  }
}
