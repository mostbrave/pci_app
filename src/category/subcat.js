'use strict';

export default class Subcat {

  constructor (subcatID, subcatName, category, thumbsUpCount, thumbsDownCount) {
    //This represents the id of the subcategory. e.g subcat33
    this.subcatID = subcatID;

    //this represents the name of the subcategory. e.g. Have you excercised?
    this.subcatName = subcatName;

    //this represents the category object
    this.category = category;

    //this represents the all time total number of thumbs up
    this.thumbsUpCount=thumbsUpCount;

    //this represents the all time total number of thumbs down
    this.thumbsDownCount=thumbsDownCount;
  }
}
