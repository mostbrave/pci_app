'use strict';

import firebase from 'firebase';
import Subcat from './subcat';
import Category from './category';

/**
 * This method retrieves all subcategories of a particular user
 * @return an array of all user subcat objects
 **/
export var getFullSubcatInfo = function(callback) {

    var catArray = new Array();
    var subcatArray = new Array();
    var ratings = new Array();
    //Get all defined categories
    var ref = firebase.database().ref('/categories/');

    //Retrieve the Category Details
    ref.once('value', function(categoriesSnapShot) {
        if (categoriesSnapShot && categoriesSnapShot.numChildren() > 0) {
            categoriesSnapShot.forEach(function(catDetails) {
                var catKey = catDetails.key;
                var catData = catDetails.val();
                catArray[catKey] = new Category(catKey, catData.text,
                    catData.desc, catData.img);
            });
            //Retrieve the Tracking Aggregates of the subcategories
            //This will store the number of Yes/No
            var trackingRef = firebase.database().ref('/tracking/' +
                firebase.auth().currentUser.uid + '/' +
                'aggregates/');
            trackingRef.once('value', function(trackingAggregates) {

                if (trackingAggregates &&
                  trackingAggregates.numChildren() > 0) {
                    trackingAggregates.forEach(function(subcatRating) {
                        var subcatID = subcatRating.key;
                        var subcatRatingDetail = subcatRating.val();
                        ratings[subcatID] = subcatRatingDetail;
                    });
                }

                var subcatRef = firebase.database().ref('/users/' +
                    firebase.auth().currentUser.uid + '/subcategories/');
                subcatRef.once('value', function(subcatSnapshot) {
                    if (subcatSnapshot && subcatSnapshot.numChildren() > 0) {
                        subcatSnapshot.forEach(function(subcatDetails) {
                            var subcatKey = subcatDetails.key;
                            var subcatData = subcatDetails.val();
                            subcatArray[subcatKey] = new Subcat(subcatKey, subcatData.name,
                                catArray[subcatData.category],
                                (typeof(ratings[subcatKey]) != "undefined"
                                &&typeof(ratings[subcatKey].yes) != "undefined"
                                &&ratings[subcatKey].yes>=0?ratings[subcatKey].yes:0),
                                (typeof(ratings[subcatKey]) != "undefined"
                                &&typeof(ratings[subcatKey].no) != "undefined"
                                &&ratings[subcatKey].no>=0?ratings[subcatKey].no:0));
                        });
                        callback(subcatArray);
                    } else {
                        callback(false);
                    }
                });

            });
        }
    });
} //end of method
