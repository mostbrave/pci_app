import React, { Component } from 'react'
import { AppRegistry } from 'react-native'
import App from './src/app'

class PCIApp extends Component {
  render() {
    return (
      <App />
    )
  }
}

AppRegistry.registerComponent('pci_app', () => PCIApp);
